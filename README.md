***GAME OF SYMBIOSIS (GOS)***
=============================

This was part of an end-of-year programming project completed as part of my conversion masters in Computer Science at Birkbeck College London, 2012-2014.
It is a cellular automata simulator written in Java, inspired by Conway's 'Game of Life': (http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

------------------------------------

**ARCHITECTURE**
----------------
A simple 3-tier architecture of presentation, logic and data.

1. The presentation is a graphical user interface (GUI) of the cellular automata.

2. The logic layer is an updating graph of vertex nodes and a rule engine.

3. The data layer has the constants that are used for the GUI, the size and update rate of the graph.

**CLASS STRUCTURE**
-------------------
The program is launched from 'Main' class. Here the 5 main objects of the program are instantiated: GUI, Graph, Simulation, UserInput and InputOutput.

GUI in turn instantiates the 3 simple factories that build the presentation layer.

Graph instantiates a default number of vertices via GraphFactory.

Simulation instantiates the visitors classes and rule engine responsible for determine each subsequent set of states.

**GUI**
-------

Javax Swing widget toolkit was used to generate the polyomino tilings format that is typical of many cellular automata simulations. The infinite lattice is presented as a 2D grid.  

I used Eclipse's GUI designer - WindowBuilder - to generate the Swing GUI.

**DESIGN PATTERNS USED**
-------------------------

The **Visitor** behavioural design pattern is used at each iteration of the simulation, the state of every vertex in the cellular automata must be updated. There are 3 classes that visit every vertex at every iteration, to sum the state of neighbours, determine the next state of each vertex and finally to update it.

The **Facade** structural design pattern is used here in the form of a single class called UserInput. All user interactions/inputs are channelled through this class which has references to the numerous other classes that are needed to respond to the user interaction.

**Simple Factory** is unofficially recognised as a design pattern. It is not the same as the GoF's creational design patterns 'Factory Method' and 'Abstract Factory'. It serves the function of encapsulating object creation and is used several times for object creation here, including instantiation and initialisation of the Graph object, the GUI components separated out as Windows, Cellular Automata and Buttons.

A **DAO** .. (data access object)


---


The conversion masters included four other programming courseworks:

Two individual-programming courseworks for the 'Programming in Java' module:

* Contact Manager											https://github.com/bbk-pij-2012-20/ContactManager.git
* Quiz game													https://github.com/bbk-pij-2012-20/Quiz.git



Two group-programming courseworks for the 'Object-Oriented Design & Programmming' module

* Simple Machine Language (SML)	- group of 2 students		https://github.com/phil1000/SMLCW.git
* Battleships game 				- group of 3 students		https://bitbucket.org/shahinzibaee/battleshipclone
