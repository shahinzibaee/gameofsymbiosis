package gos.factories;

import java.awt.Color;

/**
 * stores all the integer values that are relevant to the 
 * dimensions of the gui and its components (buttons and fields)
 * 
 * @author Shahin
 *
 */
public class GuiComponents {

	static final int MAIN_DISTANCE_FROM_LEFT = 100;
	static final int MAIN_DISTANCE_FROM_TOP = 50;
	static final int MAIN_WIDTH = 1170;
	static final int MAIN_HEIGHT = 720;		
	
	static final int CA_DISTANCE_FROM_LEFT = 20;
	static final int CA_DISTANCE_FROM_TOP = 10;
	static final int CA_WIDTH = 660;
	static final int CA_HEIGHT = 660;

	static final int GAP_BETWEEN_PANELS = 20;
	
	static final int USER_CONTROL_PANEL_DISTANCE_FROM_LEFT = CA_DISTANCE_FROM_LEFT + CA_WIDTH + GAP_BETWEEN_PANELS;
	static final int USER_CONTROL_PANEL_DISTANCE_FROM_TOP = CA_DISTANCE_FROM_TOP;
	static final int USER_CONTROL_PANEL_WIDTH = MAIN_WIDTH - CA_WIDTH - 3*GAP_BETWEEN_PANELS;
	static final int USER_CONTROL_PANEL_HEIGHT = CA_HEIGHT;

	static final int SPACE = 25;
	
	static final int TEXT_INPUT_FIELD_DISTANCE_FROM_LEFT = USER_CONTROL_PANEL_DISTANCE_FROM_LEFT + SPACE + 135;
	static final int TEXT_INPUT_FIELD_DISTANCE_FROM_TOP = USER_CONTROL_PANEL_DISTANCE_FROM_TOP + SPACE;
	static final int TEXT_INPUT_FIELD_WIDTH = 50; 
	static final int TEXT_INPUT_FIELD_HEIGHT = 40;

	static final int GAP_BETWEEN_INPUT_AND_BUTTONS = 200;

	static final int LEFT_SIDE_BUTTON_DISTANCE_FROM_LEFT = USER_CONTROL_PANEL_DISTANCE_FROM_LEFT + SPACE;
	static final int FIRST_BUTTON_DISTANCE_FROM_TOP = TEXT_INPUT_FIELD_DISTANCE_FROM_TOP;
	static final int BUTTON_WIDTH = 185; 
	static final int BUTTON_HEIGHT = TEXT_INPUT_FIELD_HEIGHT;
	
	static final int FIRST_CELL_DISTANCE_FROM_LEFT = 21; 
	static final int FIRST_CELL_DISTANCE_FROM_TOP = 11;
	static final int CELL_WIDTH = 5; 
	static final int CELL_HEIGHT = CELL_WIDTH;
	static final int GAP_BETWEEN_CELLS = 1;
	
	static final int RULE_PANEL_FROM_LEFT = CA_DISTANCE_FROM_LEFT + CA_WIDTH;
	static final int RULE_PANEL_FROM_TOP = CA_DISTANCE_FROM_TOP;
	static final int RULE_WIDTH = 20; 
	static final int RULE_HEIGHT = USER_CONTROL_PANEL_HEIGHT;
	
	static final Color MAIN_WINDOW_COLOUR = Color.GRAY;
	static final Color USER_CONTROL_PANEL_COLOUR = Color.LIGHT_GRAY;
	static final Color RULE_PANEL_COLOUR = Color.white;
	static final Color GOL_RULE_COLOUR = Color.yellow;
	static final Color GOL_RULE_MINUS_COLOUR = Color.red; 
	static final Color GOL_RULE_PLUS_COLOUR = Color.green;
	static final Color OFF_CELL_COLOUR = Color.WHITE;
	static final Color ON_CELL_COLOUR = Color.BLACK;
	static final Color TRAMBODY_CELL_COLOUR = Color.BLUE;
	static final Color TRAMHEAD_CELL_COLOUR = Color.MAGENTA;
	
	static final int COUNTER_DISTANCE_FROM_LEFT = LEFT_SIDE_BUTTON_DISTANCE_FROM_LEFT;
	static final int COUNTER_DISTANCE_FROM_TOP = 200;
	static final int COUNTER_WIDTH = TEXT_INPUT_FIELD_WIDTH * 5; 
	static final int COUNTER_HEIGHT = TEXT_INPUT_FIELD_HEIGHT * 2;
	static final String TIME_STEPS_COUNTER_LABEL_TEXT = "Number of steps:";
	static final String ON_CELL_COUNTER_LABEL_TEXT    = "Number of on-cells:";
	static final String CUMULATIVE_ON_CELL_COUNTER_LABEL_TEXT = "Sum of on-cells:";
	static final String ON_CELL_AVERAGE_TEXT = "Average on-cell trend:";		
	static final String ENERGY_COUNTER_TEXT = "Energy resources:";
	static final String ENERGY_BUFFER_COUNTER_TEXT = "Energy buffer:";
	
	static final String DIMENSIONS_LABEL_TEXT = "input 10-100, enter:";
	static final String UPDATES_LABEL_TEXT = "input 1-10, enter:";
	static final String DEFAULT_DIMENSIONS_BUTTON_TEXT = "DEFAULT DIMENSIONS";
	static final String DEFAULT_UPDATES_BUTTON_TEXT = "DEFAULT UPDATES/SEC";
	static final String START_GOL_BUTTON_TEXT = "START GAME OF LIFE";
	static final String RESET_GAME_BUTTON_TEXT = "RESET";
	static final String START_GOS_BUTTON_TEXT = "START GAME OF SYMBIOSIS";
	static final String START_WITH_TRAM_BUTTON_TEXT = "START WITH TRAM";
	static final String READ_IN_FILE_BUTTON_TEXT = "Read in config file";
	static final String WRITE_OUT_FILE_BUTTON_TEXT = "Write out config file";
	static final String STOP_BUTTON_TEXT = "PAUSE";
	
}