package gos.factories;

import gos.GUI.GUI;
import gos.interfaces.WindowFactoryIFC;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class WindowFactory implements WindowFactoryIFC{
	
	private int left;
	private int top;
	private int width;
	private int height;
	private JPanel rulePanel;//indicates which rule is being used

	/**
	 * zero-args constructor
	 */
	public WindowFactory() {}
	
	@Override
	public JPanel makeMainWindow(GUI gui) {
		
		left = GuiComponents.MAIN_DISTANCE_FROM_LEFT;
		top = GuiComponents.MAIN_DISTANCE_FROM_TOP;
		width = GuiComponents.MAIN_WIDTH;
		height = GuiComponents.MAIN_HEIGHT;
		gui.setBounds(left, top, width, height); 
		JPanel mainWindow = new JPanel();
		mainWindow.setBorder(new EmptyBorder(5, 5, 5, 5));
		gui.setContentPane(mainWindow);
		mainWindow.setLayout(null);
		mainWindow.setBackground(GuiComponents.MAIN_WINDOW_COLOUR);
		return mainWindow;
		
	}
	
	@Override
	public JPanel makeUserControlPanel() {

		left = GuiComponents.USER_CONTROL_PANEL_DISTANCE_FROM_LEFT;
		top = GuiComponents.USER_CONTROL_PANEL_DISTANCE_FROM_TOP;
		width = GuiComponents.USER_CONTROL_PANEL_WIDTH;
		height = GuiComponents.USER_CONTROL_PANEL_HEIGHT;
		JPanel userControlPanel = new JPanel();
		userControlPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		userControlPanel.setBounds(left, top, width, height);
		userControlPanel.setLayout(null);
		userControlPanel.setBackground(GuiComponents.USER_CONTROL_PANEL_COLOUR);	
		return userControlPanel;
				
	}

	@Override
	public void makeRulePanel(JPanel mainWindow) {
		
		left = GuiComponents.RULE_PANEL_FROM_LEFT;
		top =  GuiComponents.RULE_PANEL_FROM_TOP;
		width = GuiComponents.RULE_WIDTH; 
		height = GuiComponents.RULE_HEIGHT;
		rulePanel = new JPanel();
		rulePanel.setBounds(left, top, width, height);
		rulePanel.setBackground(GuiComponents.RULE_PANEL_COLOUR);
		mainWindow.add(rulePanel);
		
	}
	
	@Override
	public void setRulePanelToYellow() {
		
		rulePanel.setBackground(GuiComponents.GOL_RULE_COLOUR);
		
	}
	
	@Override
	public void setRulePanelToRed() {
		
		rulePanel.setBackground(GuiComponents.GOL_RULE_MINUS_COLOUR);
		
	}
	
	@Override
	public void setRulePanelToGreen() {
		
		rulePanel.setBackground(GuiComponents.GOL_RULE_PLUS_COLOUR);
		
	}

	/*
	 * NOT USED (yet)
	 * @return
	 *
	public JPanel makeCellularAutomataPanel() {
		
		left = GuiComponents.CA_DISTANCE_FROM_LEFT;
		top = GuiComponents.CA_DISTANCE_FROM_TOP;
		width = GuiComponents.CA_WIDTH;
		height = GuiComponents.CA_HEIGHT;
		JPanel cellularAutomataPanel = new JPanel();
		cellularAutomataPanel.setBounds(left, top, width, height);
		cellularAutomataPanel.setBackground(Color.DARK_GRAY);
		return cellularAutomataPanel;
	
	}*/
	
}