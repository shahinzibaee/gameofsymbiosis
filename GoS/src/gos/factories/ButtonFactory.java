package gos.factories;

import gos.factories.GuiComponents;
import gos.interfaces.ButtonFactoryIFC;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import gos.main.DefaultValues;
import gos.main.UserInput;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ButtonFactory implements ButtonFactoryIFC { 

	private int height, left, space, textLeft, textWidth, top, verticalGap, width;
	private UserInput userInput;
	private JTextField dimensionsOfToroidInput, updatesPerSecInput;  
	private JLabel timeStepCounter, onCellCounter, cumulativeNumOfOnCellCounter, onCellAverageCounter, energyCounter, energyBufferCounter;
	private String timeStepCounterText, onCellsCounterText, cumulativeOnCellsCounterText, onCellAverageCounterText, energyCounterText, energyBufferCounterText;
	private final static String TABx4 = "\t\t\t\t";
	private final static String TABx7 = "\t\t\t\t\t\t\t";
	private final static String TABx11 = "\t\t\t\t\t\t\t\t\t\t\t";
	private final static String TABx12 = "\t\t\t\t\t\t\t\t\t\t\t\t";
	private final static String TABx13 = "\t\t\t\t\t\t\t\t\t\t\t\t\t";
	
	public ButtonFactory()  {
		
		left = GuiComponents.LEFT_SIDE_BUTTON_DISTANCE_FROM_LEFT;
		top = GuiComponents.FIRST_BUTTON_DISTANCE_FROM_TOP; 
		width = GuiComponents.BUTTON_WIDTH;
		height = GuiComponents.BUTTON_HEIGHT;
		space = GuiComponents.SPACE;
		verticalGap = GuiComponents.GAP_BETWEEN_INPUT_AND_BUTTONS;
		textLeft = GuiComponents.TEXT_INPUT_FIELD_DISTANCE_FROM_LEFT;
		textWidth = GuiComponents.TEXT_INPUT_FIELD_WIDTH;
		timeStepCounterText = GuiComponents.TIME_STEPS_COUNTER_LABEL_TEXT;
		onCellsCounterText = GuiComponents.ON_CELL_COUNTER_LABEL_TEXT;
		cumulativeOnCellsCounterText = GuiComponents.CUMULATIVE_ON_CELL_COUNTER_LABEL_TEXT;
		onCellAverageCounterText = GuiComponents.ON_CELL_AVERAGE_TEXT;
		energyCounterText = GuiComponents.ENERGY_COUNTER_TEXT;
		energyBufferCounterText = GuiComponents.ENERGY_BUFFER_COUNTER_TEXT;
		
	}

	/**
	 * called from make ButtonsAndInputs(JPanel)
	 * @param buttonText
	 * @param addToLeft
	 * @param addToTop
	 * @param addToWidth
	 * @param addToHeight
	 * @return
	 */
	private JButton makeButton(String buttonText, int addToLeft, int addToTop, int addToWidth, int addToHeight) {
		
		JButton button = new JButton(buttonText);
		button.setBounds(left + addToLeft, top + addToTop, width + addToWidth, height + addToHeight);
		return button;		
		
	}
	
	/**
	 * called from make ButtonsAndInputs(JPanel)
	 * @param label
	 * @param defaultValue
	 * @param addToLeft
	 * @param addToTop
	 * @param addToWidth
	 * @param addToHeight
	 * @return
	 */
	private JTextField makeTextField(String defaultValue, int addToLeft, int addToTop, int addToWidth, int addToHeight) {
		
		JTextField inputTextField = new JTextField();
		inputTextField.setText(defaultValue);
		inputTextField.setBounds(textLeft + addToLeft, top + addToTop, textWidth + addToWidth, height + addToHeight);
		return inputTextField;
		
	}

	private JLabel makeLabel(String label, int addToLeft, int addToTop, int addToWidth, int addToHeight) {
		
		JLabel inputField = new JLabel();
		inputField.setText(label);
		inputField.setBounds(textLeft + addToLeft, top + addToTop, textWidth + addToWidth, height + addToHeight);
		return inputField;
		
	}
	
		@Override
	public void makeButtonsAndInputs(JPanel mainWindow) {
					
		// .....INPUT FIELDS AND DEFAULT BUTTONS......
		final JTextField dimensionsOfToroidInput = makeTextField("" + DefaultValues.DEFAULT_DIMENSIONS_OF_TOROID,0,0,0,0); 
		this.dimensionsOfToroidInput = dimensionsOfToroidInput;
		mainWindow.add(this.dimensionsOfToroidInput);
				
		int distanceToTheLeftOfInputTextField = - 130;
		int addToLeft = distanceToTheLeftOfInputTextField;
		JLabel dimensionsLabel = makeLabel(GuiComponents.DIMENSIONS_LABEL_TEXT,addToLeft,0,width,0); 
		mainWindow.add(dimensionsLabel);
		//final String dimensionsOfToroidUserInput = dimensionsOfToroidInput.getText();

		dimensionsOfToroidInput.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) { 
				
				System.out.println("Changing dimensionsOfToroid input");
				userInput.setDimensionsOfToroidTo(Integer.parseInt(dimensionsOfToroidInput.getText()));

			}			
			
		});		
		
		int addToTop = height + space;
		final JTextField updatesPerSecInput = makeTextField("" + DefaultValues.DEFAULT_UPDATES_PER_SECOND,0,addToTop,0,0); 
		this.updatesPerSecInput = updatesPerSecInput;
		mainWindow.add(this.updatesPerSecInput);
	
		JLabel updatesPerSecLabel = makeLabel(GuiComponents.UPDATES_LABEL_TEXT ,addToLeft,addToTop,width,0); 
		mainWindow.add(updatesPerSecLabel);

		updatesPerSecInput.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) { 
				
				System.out.println("Changing updatesPerSecond input");
				userInput.setUpdatesPerSecondTo(Integer.parseInt(updatesPerSecInput.getText()));
				
			}			
			
		});		
	        
		// RESET DIMENSIONS AND UPDATES PER SEC BUTTONS
		addToLeft = width + space;	
		JButton resetToDefaultDimensionsButton = makeButton(GuiComponents.DEFAULT_DIMENSIONS_BUTTON_TEXT,addToLeft,0,0,0);
		mainWindow.add(resetToDefaultDimensionsButton); 

		resetToDefaultDimensionsButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) { 
				
				System.out.println("Resetting to default dimensions");
				userInput.resetDefaultDimensions();
				
			}
			
		});	
		
		JButton resetToDefaultTimeStepButton = makeButton(GuiComponents.DEFAULT_UPDATES_BUTTON_TEXT,addToLeft,addToTop,0,0);
		mainWindow.add(resetToDefaultTimeStepButton);		

		resetToDefaultTimeStepButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) { 
				
				System.out.println("Resetting to default time");
				userInput.resetDefaultUpdatesPerSecond();
				
			}
			
		});	
		
		
		// .....COUNTERS LABELS......		
		timeStepCounter = new JLabel();
		int timeSteps = 0;
		timeStepCounter.setText(timeStepCounterText + TABx12 + timeSteps);
		timeStepCounter.setBounds(GuiComponents.COUNTER_DISTANCE_FROM_LEFT,GuiComponents.COUNTER_DISTANCE_FROM_TOP, GuiComponents.COUNTER_WIDTH, GuiComponents.COUNTER_HEIGHT);
		mainWindow.add(timeStepCounter);

		energyCounter = new JLabel();
		int energyResources = 0;
		energyCounter.setText(energyCounterText + TABx11 + energyResources);
		energyCounter.setBounds(GuiComponents.COUNTER_DISTANCE_FROM_LEFT,GuiComponents.COUNTER_DISTANCE_FROM_TOP + GuiComponents.SPACE, GuiComponents.COUNTER_WIDTH, GuiComponents.COUNTER_HEIGHT);
		mainWindow.add(energyCounter);

		energyBufferCounter = new JLabel();
		int energyBuffer = 0;
		int inLineWithResetButtons = width + space + left;
		energyBufferCounter.setText(energyBufferCounterText + TABx4 + energyBuffer);
		energyBufferCounter.setBounds(inLineWithResetButtons,GuiComponents.COUNTER_DISTANCE_FROM_TOP + GuiComponents.SPACE, GuiComponents.COUNTER_WIDTH, GuiComponents.COUNTER_HEIGHT);
		mainWindow.add(energyBufferCounter);
		
		onCellCounter = new JLabel();
		int numOfOnCells = 0;
		onCellCounter.setText(onCellsCounterText + TABx7 + numOfOnCells);
		onCellCounter.setBounds(GuiComponents.COUNTER_DISTANCE_FROM_LEFT,GuiComponents.COUNTER_DISTANCE_FROM_TOP + (GuiComponents.SPACE * 2), GuiComponents.COUNTER_WIDTH, GuiComponents.COUNTER_HEIGHT);
		mainWindow.add(onCellCounter);
		
		cumulativeNumOfOnCellCounter = new JLabel();
		int cumulativeNumOfOnCells = 0;
		cumulativeNumOfOnCellCounter.setText(cumulativeOnCellsCounterText + TABx13 + cumulativeNumOfOnCells);
		cumulativeNumOfOnCellCounter.setBounds(GuiComponents.COUNTER_DISTANCE_FROM_LEFT,GuiComponents.COUNTER_DISTANCE_FROM_TOP + (GuiComponents.SPACE * 3), GuiComponents.COUNTER_WIDTH, GuiComponents.COUNTER_HEIGHT);
		mainWindow.add(cumulativeNumOfOnCellCounter);
		
		onCellAverageCounter = new JLabel();
		onCellAverageCounter.setText(onCellAverageCounterText + TABx4 +  "0");
		onCellAverageCounter.setBounds(GuiComponents.COUNTER_DISTANCE_FROM_LEFT,GuiComponents.COUNTER_DISTANCE_FROM_TOP + (GuiComponents.SPACE * 4), GuiComponents.COUNTER_WIDTH, GuiComponents.COUNTER_HEIGHT);
		mainWindow.add(onCellAverageCounter);

		// .....START BUTTONS......				
		addToTop = 2 * height + 2 * space + verticalGap;
		int addToHeight = height;
		JButton startGoLButton = makeButton(GuiComponents.START_GOL_BUTTON_TEXT,0,addToTop,0,addToHeight);
		mainWindow.add(startGoLButton); 		

		startGoLButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) { 
				
				System.out.println("Starting Game of Life");
				userInput.startGoL();
				
			}
			
		});		
		
		int resetWidth = width / 2;
		JButton resetGameButton = makeButton(GuiComponents.RESET_GAME_BUTTON_TEXT,addToLeft,addToTop, - resetWidth,addToHeight);
		mainWindow.add(resetGameButton); 		

		resetGameButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) { 
				
				System.out.println("Resetting the game, with new random states");
				userInput.resetGame();
				
			}
			
		});	
		
		addToLeft += resetWidth;
		int pauseWidth = width / 2;
		JButton pauseButton = makeButton(GuiComponents.STOP_BUTTON_TEXT,addToLeft,addToTop, - pauseWidth,addToHeight);
		mainWindow.add(pauseButton); 		

		pauseButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) { 
				
				System.out.println("Pausing simulation");
				userInput.pauseGame();
				
			}
			
		});	

		addToTop += 2 * height + space;	
		JButton startGoSButton = makeButton(GuiComponents.START_GOS_BUTTON_TEXT,0,addToTop,0,addToHeight);
		mainWindow.add(startGoSButton);

		startGoSButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) { 
				
				System.out.println("Starting Game of Symbiosis");
				userInput.startGoS();
				
			}
			
		});
		 		
		addToLeft -= resetWidth;
		JButton startGoSWithTRAMButton = makeButton(GuiComponents.START_WITH_TRAM_BUTTON_TEXT,addToLeft,addToTop,0,addToHeight);
		mainWindow.add(startGoSWithTRAMButton);

		startGoSWithTRAMButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) { 
				
				System.out.println("Starting new Game of Symbiosis - with TRAM");
				userInput.startTRAMGoS();
				
			}
			
		});
		
		// .....INPUT OUTPUT BUTTONS......
		addToTop += (2 * height) + (2 * space);
		JButton loadButton = makeButton(GuiComponents.READ_IN_FILE_BUTTON_TEXT,0,addToTop,0,0);
		mainWindow.add(loadButton);		
		
		loadButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) { 
				
				System.out.println("Reading in from config file");
				userInput.readInFromConfigFile();
				
			}
			
		});
		
		JButton writeToConfigFileButton = makeButton(GuiComponents.WRITE_OUT_FILE_BUTTON_TEXT,addToLeft,addToTop,0,0);
		mainWindow.add(writeToConfigFileButton);	

		writeToConfigFileButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) { 
				
				System.out.println("Writing out to config file");
				userInput.writeOutToConfigFile();

			}
			
		});
		
	}
	
	@Override
	public void set(UserInput userInput) {
		
		this.userInput = userInput;

	}

	@Override
	public void set(int numOfOnCells, int cumulativeNumOfOnCells, int timeSteps, int energy, int energyBuffer) {
		
		timeStepCounter.setText(timeStepCounterText + TABx12 + timeSteps);

		if (energy != 0) {
			
			energyCounter.setText(energyCounterText + TABx11 + energy);

		
		} else {
			
			energyCounter.setText(energyCounterText + TABx11 + "NA");
			
		}

		if (energyBuffer != 0){
			
			energyBufferCounter.setText(energyBufferCounterText + TABx4 + energyBuffer);
		
		} else {
			
			energyBufferCounter.setText(energyBufferCounterText + TABx4 + "NA to GoL");
			
		}
		
		onCellCounter.setText(onCellsCounterText + TABx7 + numOfOnCells);
		cumulativeNumOfOnCellCounter.setText(cumulativeOnCellsCounterText + TABx13 + cumulativeNumOfOnCells);
		
		if (timeSteps != 0) {
				
				onCellAverageCounter.setText(onCellAverageCounterText + TABx4 + (cumulativeNumOfOnCells / timeSteps));
		
		} else {
			
				onCellAverageCounter.setText(onCellAverageCounterText+ TABx4 + "NA");
		
		}
		
	}
	
	@Override
	public void resetToDefaultDimensionsOfToroid() {

		dimensionsOfToroidInput.setText("" + DefaultValues.DEFAULT_DIMENSIONS_OF_TOROID);
		
	}
	
	@Override
	public void resetToDefaultUpdatesPerSec() {

		updatesPerSecInput.setText("" + DefaultValues.DEFAULT_UPDATES_PER_SECOND);
		
	}

}