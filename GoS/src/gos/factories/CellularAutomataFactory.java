package gos.factories;

import gos.factories.GuiComponents;
import gos.graph.Graph;
import gos.graph.Vertex;
import gos.interfaces.CellularAutomataFactoryIFC;

import java.awt.Color;

import javax.swing.JPanel;

public class CellularAutomataFactory implements CellularAutomataFactoryIFC{

	private JPanel[][] cellularAutomata;
	private int left, top, width, height, gap, addToLeft, addToTop; //positions and size of cells.  

	/**
	 * zero-arg constructor
	 */
	public CellularAutomataFactory() {
		
		left = GuiComponents.FIRST_CELL_DISTANCE_FROM_LEFT;
		top = GuiComponents.FIRST_CELL_DISTANCE_FROM_TOP;
		width = GuiComponents.CELL_WIDTH;
		height = GuiComponents.CELL_HEIGHT;
		gap = GuiComponents.GAP_BETWEEN_CELLS;
		
	}

	@Override
	public void makeCellularAutomata(Graph graph, JPanel mainWindow) {
		
		int dimensionsOfToroid = graph.getDimensionsOfToroid();
		normaliseCellSize(dimensionsOfToroid); 
		cellularAutomata = new JPanel[dimensionsOfToroid][dimensionsOfToroid];		
		Vertex currentVertex = null;
		int vertexNowState = 0;
		
		for (int ordinate = 0; ordinate < dimensionsOfToroid; ordinate++) {
			
			for (int abscissa = 0; abscissa < dimensionsOfToroid; abscissa++) {
				
				addToTop = ordinate * (gap + height);
				addToLeft = abscissa * (gap + width);
				cellularAutomata[ordinate][abscissa] = new JPanel();
				cellularAutomata[ordinate][abscissa].setBounds((left + addToLeft), (top + addToTop), width, height);
				mainWindow.add(cellularAutomata[ordinate][abscissa]);
				currentVertex = graph.getVertexWithId(convertToIdFrom(abscissa,ordinate));
				vertexNowState = convertStateToNumberFrom(currentVertex.getNowState());
				colourCell(currentVertex.getNowType(), currentVertex.getNowHeadOfTRAM(), vertexNowState, ordinate, abscissa);

			}
		
		}
		
	}
	
	/**
	 * called by makeCellularAutomata(Graph, JPanel)
	 */
	private void normaliseCellSize(int dimensionsOfToroid) {
		
		int sizeOfCellularAutomata = (width * dimensionsOfToroid) + (gap * dimensionsOfToroid - 1); 
		int sizeOfCellularAutomataPanel = GuiComponents.CA_WIDTH;
		int byAFactorOf = 2; 
		
		while (sizeOfCellularAutomata < (sizeOfCellularAutomataPanel / byAFactorOf)) {
			
			width *= byAFactorOf; 		
			height = width;
			sizeOfCellularAutomata = (width * dimensionsOfToroid) + (gap * dimensionsOfToroid - 1);
			
		} 
		
		while (sizeOfCellularAutomata > sizeOfCellularAutomataPanel) {
			
			width /= byAFactorOf;
			height = width;
			sizeOfCellularAutomata = (width * dimensionsOfToroid) + (gap * dimensionsOfToroid-1);
					
		}
		
	}
	
	@Override
	public void updateCellsToUpdated(Graph graph) {
		
		int dimensionsOfToroid = graph.getDimensionsOfToroid();
		int vertexNowState = 0;
		Vertex currentVertex = null;
		
		for (int ordinate = 0; ordinate < dimensionsOfToroid; ordinate++) {
			
			for (int abscissa = 0; abscissa < dimensionsOfToroid; abscissa++) {
				
				currentVertex = graph.getVertexWithId(convertToIdFrom(abscissa,ordinate));
				vertexNowState = convertStateToNumberFrom(currentVertex.getNowState());
				colourCell(currentVertex.getNowType(), currentVertex.getNowHeadOfTRAM(), vertexNowState, ordinate, abscissa);

			}
		
		}
			
	}
	
	/**
	 * called by makeCellularAutomata(Graph, JPanel) and updateCellsToUpdated(graph)
	 * 
	 * @param nowType
	 * @param nowHeadOfTRAM
	 * @param state
	 * @param ordinate
	 * @param abscissa
	 */
	private void colourCell(String nowType, boolean nowHeadOfTRAM, int nowState, int ordinate, int abscissa) {
       
		if (nowType.equals("BLOK")) {
					
			if (nowState == 0) {
		
				cellularAutomata[ordinate][abscissa].setBackground(GuiComponents.OFF_CELL_COLOUR);	
			
			} else {
				
				cellularAutomata[ordinate][abscissa].setBackground(GuiComponents.ON_CELL_COLOUR);
			
			}
		
		} else if (nowType.equals("TRAM")){
					
			cellularAutomata[ordinate][abscissa].setBackground(GuiComponents.TRAMBODY_CELL_COLOUR);
					
			if (nowHeadOfTRAM == true) {
						
				cellularAutomata[ordinate][abscissa].setBackground(GuiComponents.TRAMHEAD_CELL_COLOUR);
					
			}
					
		}
				
	}
	
        
	/**
	 * called by updateCellsToUpdated(Graph)
	 * 
	 * @param state_String
	 * @return
	 */
	private int convertStateToNumberFrom(String state) {

		int stateAsNumber = 0;
		
		if (state.equals("on")) {
			
			stateAsNumber = 1;
			
		}

		return stateAsNumber;
		
	}
	
	/**
	 * called from updateCellsToUpdated(Graph)
	 * 
	 * @param abscissa
	 * @param ordinate
	 * @return String the String id, a concatenation of two integers, 
	 * the abscissa and ordinate, separated by the character "x"  
	 */
	private String convertToIdFrom(int abscissa, int ordinate) {

		return "" + abscissa + "x" + ordinate;
		
	}

	@Override
	public JPanel[][] getCellularAutomata() {
		
		return cellularAutomata;
		
	}

}