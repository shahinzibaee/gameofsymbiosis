package gos.factories;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import gos.graph.Graph;
import gos.graph.Vertex;
import gos.interfaces.GraphFactoryIFC;
import gos.main.DefaultValues;

public class GraphFactory implements GraphFactoryIFC {
	
	private String[] direction8Pairs = new String[8];
	private Graph graph;
	private int dimensionsOfToroid; 
	private int energyResources;
	private int energyBuffer;
	
	/**
	 * zero-argument constructor, allows the 
	 */
	public GraphFactory() {
		
	}
	
	/**
	 * constructor
	 * @param totalNumOfVertices
	 */
	public GraphFactory(Graph graph) {
			
		set(graph);
		initDirection8Pairs();
		setDefaultDimensionsOfToroid();//sets value in both Graph and GraphFactory to default
			
	}
	
	@Override
	public void setDefaultDimensionsOfToroid() {
		
		set(DefaultValues.DEFAULT_DIMENSIONS_OF_TOROID);
		
	}
	
	/**
	 * initialises the string pairs of directions to facilitate reciprocal attachment of vertex pairs
	 */
	private void initDirection8Pairs(){
		
		direction8Pairs[0] = "N,S";
		direction8Pairs[1] = "NE,SW";
		direction8Pairs[2] = "E,W";
		direction8Pairs[3] = "SE,NW";
		direction8Pairs[4] = "S,N";
		direction8Pairs[5] = "SW,NE";
		direction8Pairs[6] = "W,E";
		direction8Pairs[7] = "NW,SE";
		
	}
		
	@Override
	public void buildGraph() {

		makeVerticesForGraph();
		attachAllNeighbourhoods();

	}
	
	/**
	 * Generates unique id strings (e.g. from "0x0" to "100x100"), each 
	 * paired with a vertex object, in a hashmap (which is stored in Graph). 
	 * 
	 */
	private void makeVerticesForGraph() {
		
		List<Vertex> vertices = new ArrayList<>();
		
		try {
		
			if (dimensionsOfToroid == 0) {
				
				throw new NullPointerException("no value given for dimensionsOfToroid"); 
				
			}
			
			for (int i = 0; i < dimensionsOfToroid; i++) {
			
				for (int j = 0; j < dimensionsOfToroid; j++) {
				
					String newId = "" + j + "x" + i;
					Vertex newVertex = new Vertex();
					newVertex.set(newId);
					newVertex.setNowState(makeRandomState());
					newVertex.setNowType("BLOK");
					newVertex.setNowHeadOfTRAM(false);
					vertices.add(newVertex);
				
				}

			}
			
		} catch (NullPointerException e) {
				
				System.out.println(e.getMessage());
				
		}
			
		graph.set(vertices);

	}
	
	/**
	 * called by makeVerticesForGraph()
	 */
	private String makeRandomState() {
		
		Random random = new Random();
		int onOrOff = random.nextInt(2);
		String randomState = "";
		
		if (onOrOff == 1) {
			
			randomState = "on";
									
		} else {
				
			randomState = "off";
	
		}
		
		return randomState;
			
	}
	

	/** 
	 * Called exclusively by buildGraph(Graph), this method iterates through
	 * the full list of vertices in the Graph and one-by-one attaches the 8
	 * adjacent vertices to each vertex, as well as the 'nextVertexInScanChain'
	 * which it does by calling attachScanChain(Vertex).
	 * 
	 * @param graph  the Graph object currently being built by GraphFactory
	 */
	private void attachAllNeighbourhoods() {

		for (Vertex vertex : graph.getVertices()) {

			for (int i = 0; i < direction8Pairs.length; i++) {

				vertex.attachNeighbourhood(direction8Pairs[i], graph);
		
			}

		attachScanChainOf(vertex); 

		}
		
	}
	
	/**
	 * Called exclusively by attachNeighbourhood(Graph), passed one vertex object at 
	 * a time from within a for-each loop in attachNeighbourhood(Graph). 
	 * 
	 * @param currentVertex    the vertex that attachNeighbourhood(Graph) is
	 *                         currently attaching adjacent vertices to.
	 */
	private void attachScanChainOf(Vertex vertex) {
		
		int last_xy = dimensionsOfToroid - 1;
		String idOfLastVertex = "" + last_xy + "x" + last_xy;
		Vertex eastVertex = vertex.getNeighbourhood().get("E");
		String id = vertex.getId();
		int xPos = id.indexOf('x');
		
		if (!id.equals(idOfLastVertex)) {
				
			if (Integer.parseInt(id.substring(0, xPos)) == last_xy) {

				eastVertex = eastVertex.getNeighbourhood().get("S");

			} 
			
			if (vertex.getNextVertexInScanChain() == null) {
			
				vertex.setNextVertexInScanChain(eastVertex);
			
			}
			
		} 
	
	}
	
	@Override
	public Graph getGraph() {
		
		return graph;
		
	}

	@Override
	public int getDimensionsOfToroid() {
		
		return dimensionsOfToroid;
		
	}
	
	@Override
	public void set(int dimensionsOfToroid) {
		
		this.dimensionsOfToroid = dimensionsOfToroid;
		graph.set(this.getDimensionsOfToroid());
			
	}
	
	@Override
	public void setEnergy() {
		
		this.energyResources = DefaultValues.ENERGY_RESOURCES;
		this.energyBuffer = DefaultValues.ENERGY_BUFFER;
		graph.setEnergy(this.getEnergyResources(), this.getEnergyBuffer());
		
	}

	@Override
	public int getEnergyResources() {
		
		return energyResources;
		
	}
	
	@Override
	public int getEnergyBuffer() {
		
		return energyBuffer;
		
	}
	
	@Override
	public void set(Graph graph) {
		
		this.graph = graph;
		
	}
	
}