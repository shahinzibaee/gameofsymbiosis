package gos.ruleEngine;

import gos.interfaces.RuleEngineIFC;

public class RuleEngine implements RuleEngineIFC {
	
	/*
	 * zero-arg constructor
	 */
	public RuleEngine() {}
	
	@Override
	public String calculateNextStateWithGoLRules(String nowState, int sumOfOnNeighbours) {
		
		String nextState = "";
		
		if (nowState.equals("on")) {

			if (sumOfOnNeighbours == 2 || sumOfOnNeighbours == 3) {

				nextState = nowState;//i.e. stays alive if 2 or 3 neighbours

			} else {
				
				nextState = "off";//otherwise it dies i.e. 0 or 1 or 4 or 5 or 6 or 7 or 8
				
			}
			
		} else if (nowState.equals("off")) {

			if (sumOfOnNeighbours == 3) {

				nextState = "on";

			} else {
				
				nextState = nowState;//i.e. no change
				
			}

		}
		
		return nextState;
		
	}
	
	@Override
	public String calculateNextStateWithGoLPLUSRules(String nowState, int sumOfOnNeighbours) {
		
		String nextState = "";
		
		if (nowState.equals("on")) {

			if (sumOfOnNeighbours == 2 || sumOfOnNeighbours == 3) {

				nextState = nowState;//i.e. stays alive if 2 or 3 alive neighbours

			} else {
				
				nextState = "off";//else it dies - this is as standard GoL rules
				
			}
			
		} else if (nowState.equals("off")) {//here's where it differs

			if (sumOfOnNeighbours == 3 || sumOfOnNeighbours == 2) {  
// if it's dead, there are now an extra condition whereby a cell would be born. 
// i.e. more scenarios of coming back to life
				nextState = "on";

			} else {
				
				nextState = nowState;
				
			}

		}
		
		return nextState;
		
	}

	@Override
	public String calculateNextStateWithGoLMINUSRules(String nowState, int sumOfOnNeighbours) {
		
		String nextState = "";
		
		if (nowState.equals("on")) {

			if (sumOfOnNeighbours == 2) { // here is the difference
// i.e. there is only 1 conditon (rather than 2) whereby the living cell will remain alive.
				nextState = nowState;

			} else {
				
				nextState = "off";
				
			}
			
		} else if (nowState.equals("off")) {

			if (sumOfOnNeighbours == 3) { // this is same as standard GoL

				nextState = "on";

			} else {
				
				nextState = nowState;//i.e. no change
				
			}

		}
		
		return nextState;
		
	}
	
}