package gos.interfaces;

import java.util.Map;

import gos.graph.Graph;
import gos.graph.Vertex;

/**
 * Represents a Vertex. Knows its own states (now & next)
 * its own types (now & next) and if it is head of a TRAM (true or false)
 * 
 * Knows which vertex is next in the 'scan chain'.
 *  
 * Knows its Moore's neighbourhood.
 * 
 * @author Shahin
 *
 */
public interface VertexIFC {

	/**
	 * Called from GraphFactory's attachNeighbourhood(Graph) to attach 
	 * a vertex to its adjacent vertex in the direction specified by the 
	 * String directionPair. All vertices are already created by this 
	 * stage and stored in Graph, so this is passed to the method. 
	 * 
	 * @param directionPair
	 * @param graph
	 */
	void attachNeighbourhood(String directionPair, Graph graph);

	/**
	 * @param vertex
	 * @return
	 */
	boolean equals(Vertex vertex);
	
	/**
	 * getter for neighbourhood
	 * @return 
	 */
	Map<String, Vertex> getNeighbourhood();

	/**
	 * getter for nowType string
	 * @return
	 */
	String getNowType();

	/**
	 * getter for next type string
	 * @return
	 */
	String getNextType();
	
	/**
	 * getter for nextVertexInScanChain pointer.
	 * @return vertex
	 */
	Vertex getNextVertexInScanChain();
	
	/**
	 * setter for nextVertexInScanChain
	 * @param vertex
	 */
	void setNextVertexInScanChain(Vertex vertex); 
	
	/**
	 * setter for current state
	 * @param state
	 */
	public void setNowState(String state);
	
	/**
	 * setter for next state of Vertex
	 * @param nextState
	 */
	public void setNextState(String nextState);
	
	/**
	 * getter for current state of Vertex
	 * @return 
	 */
	public String getNowState();
	
	/**
	 * getter for next state of Vertex
	 * @return 
	 */
	public String getNextState();

	/**
	 * setter for id 
	 * @param newId  newly generated in GraphFactory's generateIdAndVertex().
	 */
	void set(String newId);

	/**
	 * getter for id
	 * @return id string
	 */
	String getId();
	
	/**
	 * setter, called from Graph.setRandomTRAM()
	 */
	void setNowType(String nowType);

	/**
	 * setter, called from 
	 */
	void setNextType(String nextType);

	/**
	 * setter for nowHeadOfTRAM boolean
	 * @param trueOrFalse
	 */
	void setNowHeadOfTRAM(boolean trueOrFalse);

	/**
	 * setter for nextHeadOfTRAM boolean
	 * @param trueOrFalse
	 */
	void setNextHeadOfTRAM(boolean trueOrFalse);

	/**
	 * getter for nowHeadOfTRAM boolean
	 * @param trueOrFalse
	 */
	boolean getNowHeadOfTRAM();

	/**
	 * getter for nextHeadOfTRAM boolean
	 * @param trueOrFalse
	 */
	boolean getNextHeadOfTRAM();

	/**
	 * setter written for JUnit test only
	 * @param neighbourhood
	 */
	void setNeighbourhood(Map<String, Vertex> neighbourhood);

}
