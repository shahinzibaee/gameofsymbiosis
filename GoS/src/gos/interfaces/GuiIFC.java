package gos.interfaces;

import javax.swing.JPanel;

import gos.graph.Graph;
import gos.main.UserInput;

/**
 * Is the JFrame. 
 * Has the 3 factories needed to build the whole GUI.
 * 
 * @author Shahin
 *
 */
public interface GuiIFC {

	/**
	 * called by Graph.makeNewRandomStates()
	 * 
	 * called by Graph.updateCells()
	 */
	void updateCellularAutomata();

	/**
	 * called from UserInput responding to clicking button to set 
	 * the dimensionsOfToroid to a new user input
	 */
	void makeCellularAutomata();
	
	/**
	 * setter called from StateChangeVisitor via Graph
	 */
	void setRulePanelToYellow();

	/**
	 * setter called from StateChangeVisitor via Graph
	 */
	void setRulePanelToRed();

	/**
	 * setter called from StateChangeVisitor via Graph
	 */
	void setRulePanelToGreen();

	/**
	 * setter called from Main
	 * 
	 * @param graph
	 * @param userInput
	 * @param io
	 */
	void set(Graph graph, UserInput userInput);

	/**
	 * setter for graph
	 * @param graph
	 */
	void set(Graph graph);

	/**
	 * getter for JUnit
	 * @return
	 */
	Graph getGraph();

	/** 
	 * getter called from UserInput setDimensionsOfToroid(int) to enable
	 * user to input a different CA size and have it replace the 
	 * current one. Hence need to remove the old CA from the main
	 * window. (not working properly yet though)
	 * 
	 * @return
	 */
	JPanel getMainWindow();

}
