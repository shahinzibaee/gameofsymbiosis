package gos.interfaces;

import javax.swing.JPanel;

import gos.main.UserInput;

/**
 * makes the Buttons and InputTextFields
 * 
 * instantiated in GUI's constructor
 * @author Shahin
 *
 */
public interface ButtonFactoryIFC {

	/**
	 * called from GUI's constructor
	 * 
	 * @param mainWindow
	 */
	void makeButtonsAndInputs(JPanel mainWindow);

	void set(UserInput userInput);

	/**
	 * called from GUI.updateCellularAutomata()
	 * 
	 * @param numOfOnCells
	 * @param cumulativeNumOfOnCells
	 * @param timeSteps
	 * @param energy
	 * @param energyBuffer
	 */
	void set(int numOfOnCells, int cumulativeNumOfOnCells, int timeSteps,
			int energy, int energyBuffer);

	/**
	 * called from ButtonFactory, via UserInput, via GUI ! 
	 * It could just as well bypass the the other two classes because
	 * DefaultValues are public.
	 * But because the button click is a user input by definition, 
	 * The code is written to reflect this and therefore passes through
	 * UserInput and back to itself.
	 */
	void resetToDefaultDimensionsOfToroid();

	/**
	 * called from ButtonFactory, via UserInput, via GUI ! 
	 * It could just as well bypass the the other two classes because
	 * DefaultValues are public.
	 * But because the button click is a user input by definition, 
	 * The code is written to reflect this and therefore passes through
	 * UserInput and back to itself.
	 */
	void resetToDefaultUpdatesPerSec();

}
