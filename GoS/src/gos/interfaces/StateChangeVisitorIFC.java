package gos.interfaces;

import java.util.Map;

import gos.graph.Graph;
import gos.graph.Vertex;

public interface StateChangeVisitorIFC {

	/**
	 * Called by UpdateStateVisitor.updateNextStates()
	 * 
	 * Sets the next states for each vertex.
	 * 
	 * Must first call calculateNextStateOfCurrentVertex(Vertex,Map) to 
	 * calculate what the next states should be set to.
	 * 
	 * @param graph
	 */
	void updateNextStates(Map<Vertex, Integer> sumsOfNeighbourhoods);

	/**
	 * Called by UpdateStateVisitor.updateNextStatesTypes()
	 * 
	 * Sets the next states and types for each vertex.
	 * 
	 * Must first call calculateNextStateOf(Vertex, Map) 
	 * and calculateNextTypeOf(Vertex, Map) to calculate what the next
	 * states and types should be set to.
	 * 
	 * @param graph
	 */
	void updateNextStatesTypes(Map<Vertex, Integer> sumsOfNeighbourhoods,
			Map<Vertex, Integer> sumsOfNeumTRAMNeighbours);

	/**
	 * setter
	 * @param graph
	 */
	void set(Graph graph);

}
