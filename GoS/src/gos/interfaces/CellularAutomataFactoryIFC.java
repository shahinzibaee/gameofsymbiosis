package gos.interfaces;

import gos.graph.Graph;

import javax.swing.JPanel;

/**
 * makes the cellular automata, a 2D array of JPanels.
 * created in GUI's constructor
 * @author Shahin
 *
 */
public interface CellularAutomataFactoryIFC {

	/**
	 * called from Main, via GUI's makeCellularAutomata() method.
	 * 
	 * makes the cellular automata grid out of panels in a double for-loop
	 * and sets random states to 
	 * 
	 * @param mainWindow
	 */
	
	/**
	 * called from Main, via GUI's makeCellularAutomata() method.
	 * 
	 * makes the cellular automata grid out of panels in a double for-loop
	 * 
	 * @param graph
	 * @param mainWindow
	 */
	void makeCellularAutomata(Graph graph, JPanel mainWindow);

	/**
	 * called by GuiFactory.updateCells()
	 * @param graph
	 * @return
	 */
	void updateCellsToUpdated(Graph graph);

	/**
	 * called from GUI
	 * @return
	 */
	JPanel[][] getCellularAutomata();

}
