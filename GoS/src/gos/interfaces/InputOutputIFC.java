package gos.interfaces;

import gos.graph.Graph;

/**
 * responsible for encoding and decoding objects from code to and from
 * XML file on disk
 * 
 * @author Shahin
 *
 */
public interface InputOutputIFC {

	/**
	 * called from user clicking on read in file button in ButtonFactory,
	 * via UserInput.
	 * 
	 * XMLDecoder converts a file to an object. 
	 * Hence it "reads the XML file in .." to this package
	 */
	void readConfigurationFileIn();

	
	/** called from user clicking on read in file button in ButtonFactory,
	 * via UserInput.
	 * 
	 * XMLEncoder converts an object to a file. 
	 * The encode javabean converts the java object into a stream
	 * of bytes which it outputs to a file in the current directory.
	 * Hence it "writes the file out.."
	 */
	void writeConfigurationFileOut(Graph graph);

}
