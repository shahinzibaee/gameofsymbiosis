package gos.interfaces;

/**
 * Rule sets hard-coded here.
 * 
 * performs the calculation of next state for StateChangeVisitor
 * 
 * Currently holding Game Of Life Rule Set and two very simple derivate 
 * rule sets: GoLMINUS and GoLPLUS invented by me (though they may well
 * have been invented already). 
 * @author Shahin
 *
 */
public interface RuleEngineIFC {

	/**
	 * STANDARD GAME OF LIFE RULESET
	 * 
	 * called by StateChangeVisitor.getNextState()
	 * 
	 * Calculating the next state according to GoL rules: an 'on' 
	 * cell only transitions if 2 or 3 'on' neighbours
	 * an 'off' cell only transitions if 3 'on' neighbours.
	 *  
	 * @param state
	 * @param sumOfOnNeighbours
	 * @return
	 */
	String calculateNextStateWithGoLRules(String nowState, int sumOfOnNeighbours);

	/**
	 * MODIFIED GAME OF LIFE RULESET - TO PROMOTE OFF-TO-ON TRANSITIONS 
	 * (mimicking reproduction)
	 * 
	 * called by StateChangeVisitor.getNextState()
	 * 
	 * Calculating the next state according to modified GoL rules to 
	 * promote off cells turning on:
	 * 
	 * an 'off' cell transitions if 3 OR 2 'on' neighbours.
	 *  
	 * @param state
	 * @param sumOfOnNeighbours
	 * @return
	 */
	String calculateNextStateWithGoLPLUSRules(String nowState,
			int sumOfOnNeighbours);

	/**
 	 * MODIFIED GAME OF LIFE RULESET - TO PROMOTE ON-TO-OFF TRANSITIONS 
 	 * (mimicking starvation)
 	 * 
	 * called by StateChangeVisitor.getNextState()
	 * 
	 * Calculating the next state according to modified GoL rules to 
	 * promote on cells turning off:
	 * 
	 * an 'on' cell will only stay on if it has 2 'on' neighbours.
	 *  
	 * @param state
	 * @param sumOfOnNeighbours
	 * @return
	 */
	String calculateNextStateWithGoLMINUSRules(String nowState,
			int sumOfOnNeighbours);

}
