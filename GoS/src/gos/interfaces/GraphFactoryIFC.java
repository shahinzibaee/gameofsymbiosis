package gos.interfaces;

import gos.graph.Graph;

/**
 * Makes the Graph's vertices,
 * assigns their unique id strings to each,
 * gives each a random starting state (on or off)
 * attaches all the vertices to their adjacent vertices
 * by way of assigning their Moore's neighbourhood to each.
 * 
 * @author Shahin
 */
public interface GraphFactoryIFC {

	/**
	 * called from Main.initGoS() 
	 */
	void buildGraph();

	/**
	 * called from GraphFactory's constructor.
	 *  
	 * also called from ButtonFactory.resetDimensionsButton() via
	 * UserInput 
	 */
	void setDefaultDimensionsOfToroid();

	/**
	 * called by UserInput with default values.
	 * 
	 * @param dimensionOfToroid
	 */
	void set(int dimensionsOfToroid);

	/**
	 * Called from ButtonFactory if user clicks on start GoS or start with TRAM 
	 * buttons, (via UserInput.startGoS() and startTRAMGoS()) 
	 * Also called from clicking 'reset default cells' button 
	 */
	void setEnergy();

	/**
	 * getter for energy resources
	 * @return
	 */
	int getEnergyResources();

	/**
	 * setter for graph 
	 * @param graph
	 */
	void set(Graph graph);
	
	/**
	 * getter for dimensionsOfToroid
	 * @return
	 */
	int getDimensionsOfToroid();

	/**
	 * getter for graph
	 * @return
	 */
	Graph getGraph();

	/**
	 * getter for energy buffer
	 * @return
	 */
	int getEnergyBuffer();

}
