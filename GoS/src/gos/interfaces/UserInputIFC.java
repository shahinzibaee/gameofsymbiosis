package gos.interfaces;

import gos.GUI.GUI;
import gos.factories.GraphFactory;
import gos.graph.Graph;
import gos.main.InputOutput;
import gos.main.Simulation;


/**
 * performs role of passing on all user inputs (button clicks, text inputs)
 * to the appropriate class. (Similar to Facade pattern) 
 * 
 * @author Shahin
 *
 */
public interface UserInputIFC {

	/**
	 * called by user input clicking on "Start Gos" button
	 */
	void startGoS();

	/**
	 * called by user input clicking on "Start Gos" button
	 */
	void startGoL();

	/**
	 * called from ButtonFactory upon user clicking the 
	 * "Start GoS with TRAM" button
	 */
	void startTRAMGoS();

	/**
	 * called by user clicking reset button, via User Input.
	 * Responsible for reseting all the counters: 
	 * 1. timeSteps & on-cell numbers in Graph (directly)
	 * 2. timeSteps in Simulation (directly)
	 * 3. on-cell numbers in the Visitors (via Simulation).
	 * 
	 */
	void resetGame();
	
	/**
	 * stops the simulation by cancelling the timer in simulation
	 * Acts like a pause button, as start will restart from where
	 * the CA left off at time when stop button was clicked
	 */
	void pauseGame();
	
	
	/**
	 * called from clicking reset default dimensions button (ButtonFactory)
	 */
	void resetDefaultDimensions();

	/**
	 * called from clicking reset updates per sec button (ButtonFactory)
	 */
	void resetDefaultUpdatesPerSecond();
	
	/**
	 * called from clicking read in file button 
	 */
	void readInFromConfigFile();
	
	/*
	 * called from clicking read in file button 
	 */
	void writeOutToConfigFile();

	/**
	 * takes the input integer that a user has overwritten the 
	 * default value with and sends it to the GraphFactory.
	 */
	void setDimensionsOfToroidTo(int userInputDimensionsOfToroid);
	
	/**
	 * takes the input integer that a user has overwritten the 
	 * default value with and sends it to the Simulation.
	 */
	void setUpdatesPerSecondTo(int updatesPerSecond);

	/**
	 * setter called from constructor
	 * @param graph
	 */
	void set(Graph graph);
	
	/**
	 * setter called from constructor
	 * @param graphFactory
	 */
	void set(GraphFactory graphFactory);

	/**
	 * setter for gui, called from Main 
	 * @param graphFactory
	 */
	void set(GUI gui);

	/**
	 * setter for simulation
	 * @param simulation
	 */
	void set(Simulation simulation);

	/**
	 * getter
	 * @return
	 */
	int getDefaultNumOfVertices();

	/**
	 * setter called by Main after instantiating InputOutput
	 * @param io
	 */
	void set(InputOutput io);

	/**
	 * getter for JUnit
	 * @return
	 */
	Graph getGraph();

	/**
	 * getter for JUnit
	 * @return
	 */
	GraphFactory getGraphFactory();

	/**
	 * getter only for JUnit
	 * @return
	 */
	Simulation getSimulation();

}