package gos.interfaces;

import gos.GUI.GUI;
import gos.factories.GraphFactory;
import gos.graph.Vertex;

import java.util.List;

/**
 * Represents a Graph. Holds a list of all the vertices.
 * Also stores its size (as the dimensionsOfToroid integer).
 * Responsible for creating a randomly positioned TRAM.
 *  
 * @author Shahin
 */
public interface GraphIFC {
	
	/**
	 * called from UpdateStateVisitor.update()
	 */
	void updateCells();

	/**
	 * called from Main.initGoS()
	 */
	void buildGraph();

	/**
	 * returns a single vertex object from the list of 
	 * vertices in Graph according to its unique id.
	 * 
	 * @param id the id string of the vertex of interest
	 * @return the vertex object that has the id string 
	 */
	Vertex getVertexWithId(String id);


 	/** 
 	 * string version of graph showing just the now or next states depending on
 	 * which string argument is passed to the method.
 	 * @param state   "nowStates" or "nextStates"
 	 * @return String 
 	 */
	String toString(String state);

	/**
	 * called from ButtonFactory (click RESET button), via reset() in User Input.
	 * 
	 * sets state of every vertex in graph to "on" or "off" using Random.nextInt()
	 * 
	 *  (in GraphFactory's makeVerticesFor(Graph)).
	 */
	void makeNewRandomStates();
	
	/**
	 * called from StateChangeVisitor
	 * 
	 * @param string
	 */
	void setRulePanelToYellow();

	/**
	 * called from StateChangeVisitor
	 */
	void setRulePanelToRed();

	/**
	 * called from StateChangeVisitor 
	 */
	void setRulePanelToGreen();

	/**
	 * resets the numOfOnCells and cumulativeNumOfOnCells to 0
	 * resets timeSteps to 0
	 */
	void resetCounters();
	
	/**
	 * called from every TimerTask in Simulation 
	 */
	void incrementTimeSteps();

	/**
	 * setter
	 * @param dimensionsOfToroid
	 */
	void set(int dimensionsOfToroid);
	/**
	 * setter
	 * @param vertices
	 */
	void set(List<Vertex> vertices);
	
	/**
	 * getter for list of vertices
	 * @return list of all vertices in Graph
	 */
	List<Vertex> getVertices();

	/**
	 * getter for dimensionOfToroid integer
	 * @return the integer dimensionOfToroid
	 */
	int getDimensionsOfToroid();

	/**
	 * setter
	 * called from GraphFactory
	 */
	void setEnergyResources(int energyResources);

	/**
	 * getter
	 * called from StateChangeVisitor
	 */
	int getEnergyResources();

	/**
	 * setter
	 * @param gui
	 */
	void set(GUI gui);

	/**
	 * getter
	 * @return
	 */
	GraphFactory getGraphFactory();

	/**
	 * getter 
	 * @return
	 */
	int getNumOfOnCells();

	/**
	 * setter
	 * @param timeSteps
	 */
	void setTimeSteps(int timeSteps);

	/**
	 * setter called from StateCollectionVisitor
	 * 
	 * @param numOfOnCells
	 * @param cumulativeNumOfOnCells
	 */
	void set(int numOfOnCells, int cumulativeNumOfOnCells);

	/**
	 * getter for cumulativeNumOfOnCells, called from simulation calling 
	 * graph's updateCells, which calls GUI updateCells, which calls Graph
	 * again for the numOfOnCells. This is so GUI can set the values in
	 * via ButtonFactory.
	 *  
	 * @return
	 */
	int getCumulativeNumOfOnCells();

	/**
	 * called from GUI to set in ButtonFactory every step
	 * @return
	 */
	int getTimeSteps();

	/**
	 * called from GraphFactory
	 * setter for energy resources and energy buffer
	 * @param resources
	 * @param buffer
	 */
	void setEnergy(int resources, int buffer);

	/**
	 * getter for energy buffer
	 * @return
	 */
	int getEnergyBuffer();

	/**
	 * setter for energy buffer
	 * @return
	 */
	void setEnergyBuffer(int buffer);

}
