package gos.interfaces;

import java.util.Map;

import gos.graph.Vertex;

public interface StateCollectionVisitorIFC {

	/**
	 * called by UpdateStateVisitor.updateNextStates()
	 * 
	 * calculated for determining starvation status
	 * 
	 * @return
	 */
	int getCumulativeNumOfOnCells();

	/**
	 * called from UpdateStateVisitor's constructor once
	 * and repeatedly at the end of updateNowStates() and 
	 * updateNowStateTypes() in UpdateStateVisitor.
	 * 
	 * @return
	 */
	Map<Vertex, Integer> makeSumsOfNeighbourhoods();


	/**
	 * called from resetGame button in ButtonFactory via UserInput via
	 * Simulation, via UpdateStateVisitor !
	 */
	void resetCumulativeOnCellsToZero();

}
