package gos.interfaces;

import gos.graph.Graph;
import gos.visitors.UpdateStateVisitor;

/**
 * holds the code for periodically calling the updates of the vertices' 
 * states (and types), calculating next states (and types) and updating 
 * the GUI's cells to represent them accordingly.
 * Has a TimerTask/Timer for the repeated calls.
 * 
 * @author Shahin
 *
 */
public interface SimulationIFC {

	/**
	 * called from ButtonFactory (clicking start button) via UserInput.
	 * 
	 * Creates Timer object and TimerTask object. 
	 * 
	 * Time interval between updates in milliseconds is passed to Timer's schedule.
	 * 
	 * TimerTask launches a thread in which updates are performed by calling update().
	 *  
	 */
	void startUpdating();

	/**
	 * called from buttonFactory start GoS with TRAM, via UserInput
	 * 
	 * performs same function as startUpdating() except that it calls 
	 * methods which check and update the vertex types (for TRAM GoS)
	 * 
	 */
	void startUpdatingWithTRAM();

	/**
	 * called from resetting the game
	 */
	void cancelTimer();

	/**
	 * called from UserInput resetGame button 
	 * reset cumulativeNumOfOnCells in StateCollectionVisitor to zero
	 */
	void resetCumulativeOnCellsToZero();
		
	/**
	 * setter
	 * called by Main's initialiseGoS() method
	 */
	void setDefaultUpdatesPerSecond();

	/**
	 * setter for updatesPerSecond (millisecs integer)
	 * @param millisecs
	 */
	void set(int updatesPerSecond);
	
	/**
	 * getter for updateStateVisitor
	 * called from clicking reset button (ButotnFactory) via 
	 * UserInput.reset()  
	 * @return
	 */
	UpdateStateVisitor getUpdateStateVisitor();

	/**
	 * setter for graph
	 * called from constructor and JUnit
	 * @param graph
	 */
	void set(Graph graph);

	/**
	 * getter for JUnit 
	 * @return int millisecsBetweenUpdates
	 */
	int getMillisecsBetweenUpdates();

}