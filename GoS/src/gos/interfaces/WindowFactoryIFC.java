package gos.interfaces;

import gos.GUI.GUI;

import javax.swing.JPanel;

/**
 * Makes the main GUI window onto which the cellular automata and user
 * control and rule status panels are added. 
 * Makes the user control panel (onto which the buttons and input fields 
 * are added. 
 * Makes the rule status panel. 
 * 
 * @author Shahin
 *
 */
public interface WindowFactoryIFC {

	/**
	 * called from GUI constructor
	 * 
	 * @param guiFactory
	 * @return
	 */
	JPanel makeMainWindow(GUI gui);

	/**
	 * called from GUI constructor
	 * @return
	 */
	JPanel makeUserControlPanel();

	/**
	 * called from GUI constructor
	 * @return
	 */
	void makeRulePanel(JPanel mainWindow);

	/**
	 * called from StateChangeVisitor via Graph via GUI 
	 * for standard GoL rule set
	 * 
	 * @param color
	 */
	void setRulePanelToYellow();

	/**
	 * called from StateChangeVisitor via Graph via GUI 
	 * for GoLMINUS rule set (promoting on-to-off transitions)
	 * 
	 * @param color
	 */
	void setRulePanelToRed();

	/**
	 * called from StateChangeVisitor via Graph via GUI 
	 * for GoLPLUS rule set (promoting off-to-on transitions)
	 * 
	 * @param color
	 */
	void setRulePanelToGreen();

}
