package gos.main;

import java.awt.EventQueue;
import java.io.File;

import gos.GUI.GUI;
import gos.graph.Graph;

public class Main {
	
	public static void main(String[] args) {
		
		Main main = new Main();
		main.initGoS();
	
	}
		
	/**
	 * called from PSVM
	 * 
	 * Launches the program inside Swing's Event Dispatch Thread.
	 * 
	 */
	private void initGoS() {

		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
							
				try {
							
					GUI gui = new GUI();
					gui.setVisible(true);
					Graph graph = new Graph();	
					graph.buildGraph();
					Simulation simulation = new Simulation(graph);  
					InputOutput io = new InputOutput();
					UserInput userInput = new UserInput(graph, io, simulation);	
					gui.set(graph, userInput);
					gui.makeCellularAutomata();
					graph.set(gui);
					userInput.set(gui);
				//	io.writeConfigurationFileOut(graph);
										
				} catch (Exception e) {
				
					e.printStackTrace();
				
				}
		
			}
		
		});
		
	}
	
}