package gos.main;

import gos.graph.Graph;
import gos.graph.Vertex;
import gos.interfaces.SimulationIFC;
import gos.visitors.UpdateStateVisitor;

import java.awt.EventQueue;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class Simulation implements SimulationIFC {

	private Graph graph;
	private UpdateStateVisitor updateStateVisitor;
	private int millisecsBetweenUpdates;
	private Timer timer;

	/**
	 * constructor, instantiates the 3 visitor via UpdateStateVisitor
	 * @param graph
	 */
	public Simulation(Graph graph) {
	
		set(graph);
		updateStateVisitor = new UpdateStateVisitor(graph);
		setDefaultUpdatesPerSecond();
		
	}
	
	@Override
	public void startUpdating() {

		TimerTask timerTask = new TimerTask() {

			public void run() {

				graph.incrementTimeSteps();
				updateFromTimer();

			}
			
		};
		
		timer = new Timer();
		timer.schedule(timerTask, 0, millisecsBetweenUpdates);		
	
	}

	/**
	 * Called repeatedly from startUpdating(). 
	 * 
	 * Calls UpdateStateVisitor.updateStatesAndCells()
	 */
	private void updateFromTimer() {
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
							
				try {
					
					updateStateVisitor.updateStatesAndCells();//updates now states, next states and the GUI as well (Chain of Responsibility)
			
				} catch (Exception e) {
				
					e.printStackTrace();
			
				}
	
			}
	
		});

	}
	
	@Override
	public void startUpdatingWithTRAM() {

		TimerTask timerTask = new TimerTask() {

			public void run() {

				graph.incrementTimeSteps();
				updateWithTRAMFromTimer();

			}
			
		};
		
		timer = new Timer();
		timer.schedule(timerTask, 0, millisecsBetweenUpdates);		
	
	}
	
	/**
	 * called repeatedly from startUpdatingWithTRAM()
	 */
	private void updateWithTRAMFromTimer() {
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
							
				try {
					
					updateStateVisitor.updateStatesTypesAndCells();
			
				} catch (Exception e) {
				
					e.printStackTrace();
			
				}
	
			}
	
		});

	}
		
	@Override
	public void cancelTimer() {
		
		timer.cancel();
		
	}
	
	@Override
	public void setDefaultUpdatesPerSecond() {
		
		set(DefaultValues.DEFAULT_UPDATES_PER_SECOND);
		
	}
	
	@Override
	public void set(int updatesPerSecond) {
		
		try {
			
			if (updatesPerSecond == 0) {
				
				throw new IllegalArgumentException("You can't have 0 updates per second");
				
			}
	
			this.millisecsBetweenUpdates = 1000 / (updatesPerSecond);
		
		} catch (IllegalArgumentException e) {
			
			System.out.println(e.getMessage());
			
		}
			
	}
		
	/**
	 * 
	 * @return
	 */
	public Graph getGraph() {
		
		return graph;
	
	}
	
	@Override
	public int getMillisecsBetweenUpdates() {
		
		return millisecsBetweenUpdates;
	
	}
	
	@Override
	public UpdateStateVisitor getUpdateStateVisitor() {
		
		return updateStateVisitor;
		
	}
	
	@Override
	public void set(Graph graph) {
		
		this.graph = graph;
		
	}
	
	@Override
	public void resetCumulativeOnCellsToZero() {

		updateStateVisitor.resetCumulativeOnCellsToZero();
			
	}

	public void addTypesAndOneTRAM() {
		
		updateStateVisitor.addTypesAndOneTRAM();
		
	}

}