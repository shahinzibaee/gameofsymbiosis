package gos.main;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * stores all the default integer values required to build the 
 * graph and cellular automata, GoS, TRAM,  
 * 
 * @author Shahin
 *
 */
public class DefaultValues {
	
	public static final int DEFAULT_DIMENSIONS_OF_TOROID = 20;//94; 
	public static final int DEFAULT_UPDATES_PER_SECOND = 1;//100;
//	public static final int DEFAULT_DIMENSIONS_OF_BLOK = 10;
//	public static final int DEFAULT_DIMENSIONS_OF_TRAK = 1;// i.e. the 'width'
//	public static final int DEFAULT_DIMENSIONS_OF_TRAMKAR = 1;// i.e. one cell
//	TRAMs are any number of TRAMKARs, hence the length is undefined
	public static final int ENERGY_RESOURCES = (DEFAULT_DIMENSIONS_OF_TOROID*DEFAULT_DIMENSIONS_OF_TOROID) / 5;
	public static final int ENERGY_BUFFER = ENERGY_RESOURCES / 5; 
	public static final int TRAM_LENGTH = DEFAULT_DIMENSIONS_OF_TOROID / 5;	

}
