package gos.main;

import gos.graph.Graph;
import gos.graph.Vertex;
import gos.interfaces.InputOutputIFC;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

public class InputOutput implements InputOutputIFC {
	
	private Graph graph;
	
	/**
	 * zero-arg constructor
	 */
	public InputOutput() {}

	/**
	 * constructor
	 */
	public InputOutput(Graph graph) {
		
		this.graph = graph;
		
	}
	
	@Override
	public void readConfigurationFileIn() {

		try {

			JAXBContext jc = JAXBContext.newInstance(Graph.class);
			Unmarshaller ums = jc.createUnmarshaller();
			Graph graph = (Graph) ums.unmarshal(new File("src/graphXml/graph.xml"));
			graph.getVertexWithId("0x0");

		} catch (JAXBException e) {

			System.out.println(e.getMessage());

		}

						
	}

	//marshalling graph object to xml
	@Override
	public void writeConfigurationFileOut() {
		
		try {
			
			JAXBContext jc = JAXBContext.newInstance(Graph.class);
			Marshaller ms = jc.createMarshaller();
			ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			ms.marshal(graph, System.out);
			ms.marshal(graph, new File("src/graphXml/graphToXml.xml"));   ;
			
		} catch (JAXBException e) {
			
			System.out.println(e.getMessage());
		
		}

	}
	
}