package gos.main;

import gos.GUI.GUI;
import gos.factories.GraphFactory;
import gos.graph.Graph;
import gos.interfaces.UserInputIFC;

public class UserInput implements UserInputIFC {

	private Graph graph;
	private GraphFactory graphFactory;
	private Simulation simulation;
	private GUI gui;
	private 	InputOutput io;
	
	/**
	 * constructor
	 * @param graph
	 * @param io
	 * @param simulation
	 */
	public UserInput(Graph graph, InputOutput io, Simulation simulation) {
		
		set(graph);
		set(graph.getGraphFactory());
		set(simulation);	
		this.io = io;
				
	}
	

	@Override
	public void startGoL() {
		
		simulation.startUpdating();	
	
	}
	
	@Override
	public void startGoS() {
		
		graphFactory.setEnergy();
		simulation.startUpdating();
	
	}

	
	@Override
	public void startTRAMGoS() {
		
		graphFactory.setEnergy();
		simulation.addTypesAndOneTRAM();
		gui.updateCellularAutomata();
		simulation.startUpdatingWithTRAM();
		
	}
	
	@Override
	public void pauseGame() {
		
		simulation.cancelTimer();
		
	}

	@Override
	public void resetGame() {

		simulation.cancelTimer();
		graph.makeNewRandomStates();
		resetCounters();
		simulation.getUpdateStateVisitor().updateNextStates();
		gui.updateCellularAutomata(); 	
	
	}
		
	public void resetCounters() {
	
		graph.resetCounters();
		simulation.resetCumulativeOnCellsToZero();
				
	}

	@Override
	public void setDimensionsOfToroidTo(int userInputDimensionsOfToroid) {
				
		Graph newGraph = new Graph();
		graphFactory = new GraphFactory(newGraph);
		graphFactory.set(userInputDimensionsOfToroid);
		graphFactory.buildGraph();
		simulation = new Simulation(newGraph);
		gui.set(newGraph);
		newGraph.set(gui);
		gui.makeCellularAutomata();
	
	}

	@Override
	public void setUpdatesPerSecondTo(int updatesPerSecond) {
		
		simulation.set(updatesPerSecond);
		
	}

	@Override
	public void resetDefaultDimensions() {

		graphFactory.setDefaultDimensionsOfToroid();
		gui.resetToDefaultDimensionsOfToroid();
		
	}
 
	@Override
	public void resetDefaultUpdatesPerSecond() {
		
		simulation.setDefaultUpdatesPerSecond();
		gui.resetToDefaultUpdatesPerSec();
	
	}
		 
	@Override
	public void readInFromConfigFile() {
		
		io.readConfigurationFileIn();		
	
	}
	
	@Override
	public void writeOutToConfigFile() {
	
		io.writeConfigurationFileOut();

	}
		
	
	@Override
	public Graph getGraph() {
		
		return graph;
		
	}
	
	@Override
	public GraphFactory getGraphFactory() {
		
		return graphFactory;
		
	}
	
	@Override
	public Simulation getSimulation() {
		
		return simulation;
		
	}
	
	
	@Override
	public void set(Graph graph) {
		
		this.graph = graph;
		
	}
	
	@Override
	public void set(GraphFactory graphFactory) {
	
		this.graphFactory = graphFactory;
	
	}
	
	@Override
	public void set(GUI gui) {
		
		this.gui = gui;
	
	}

	@Override
	public void set(Simulation simulation) {
		
		this.simulation = simulation;
	
	}
	
	@Override
	public int getDefaultNumOfVertices() {
		
		return graphFactory.getDimensionsOfToroid();
		
	}
	
	@Override
	public void set(InputOutput io) {
	
		this.io = io;
		
	}

	
	/* Not currently using this code.. related to button actionPerformed
	 * 
	 * holds code for outcome of clicking one of the buttons. The identity 
	 * of the button is given by the name, as a string.
	 * 
	 * @param button
	 *
	public void getResponseToClicking(String button) {
		System.out.println("line 44 in UserInput.getResponseToClicking(String)");
		switch (button) {
		
			case "resetDefaultDimensions" : graphFactory.setDefaultDimensionsOfToroid(); break;
			case "resetDefaultRate" : simulation.setDefaultUpdatesPerSecond(); break;
			case "remakeRandomStates" : graph.setNewRandomStates(); System.out.print("line 49 in UserInput.setNewRandomStates...");gui.updateCells();System.out.println("line 49 in UserInput.setNewRandomStates after GUI.updateCells"); break; 
			case "readInFile" : io.readConfigurationFileIn(); break;
			case "startGoS" : simulation.startUpdating(); break;
			case "writeOutFile" : io.writeConfigurationFileOut(graph); break;
			default : throw new IllegalArgumentException("that button doesn't exist!");
						
		}
		
	}
	
	public void assignActionsTo(final JButton buttonClicked) {
		
		buttonClicked.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent e) { 
			
				System.out.println("Button clicked was: "+buttonClicked);
				getResponseToClicking(""+buttonClicked);
							
			}
		
		});	
	
	}*/

}