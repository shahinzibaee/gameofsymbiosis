package gos.graph;

import gos.interfaces.VertexIFC;

import java.util.HashMap;
import java.util.Map;

public class Vertex implements VertexIFC {

	private String nowState; // can be 1 of 3 states: "off", "off_to_on" or "on"
	private String nextState; // state of this vertex in next time step
	private Map<String,Vertex> neighbourhood;// 8 adjacent vertices "N", "NE", "E".. etc. 
	private Vertex nextVertexInScanChain;// the 'East' neighbour also assigned as the next vertex in a 'scan chain'
	private String id;//unique id for each vertex (also held in idAndVertex hashmap in Graph). Contains positional information.
	private String nowType;//BLOK, TRAM
	private String nextType;//BLOK, TRAM
	private boolean nowHeadOfTRAM;
	private boolean nextHeadOfTRAM;
	
	/**
	 * zero-arg constructor
	 */
	public Vertex() {
		
		neighbourhood = new HashMap<>();
		
	}
	
	@Override
	public void attachNeighbourhood(String directionPair, Graph graph) {
		
		String[] directionPairSplit = directionPair.split(",");

		if (neighbourhood.get(directionPairSplit[0]) == null) {
		
			neighbourhood.put(directionPairSplit[0], identifyAdjacentVertex(directionPairSplit[0],graph));
		
		}

		String oppositeDirection = directionPairSplit[1];
		
		if (identifyAdjacentVertex(directionPairSplit[0],graph).getNeighbourhood().get(oppositeDirection) == null) {

			identifyAdjacentVertex(directionPairSplit[0],graph).getNeighbourhood().put(oppositeDirection,this);
		
		}
		
	}
	
	/**
	 * called from attachNeighbourhood(String, Graph)
	 * 
	 * returns the vertex object which has the id of the corresponding 
	 * adjacent vertex. The adjacent vertices is pre-determined by their 
	 * unique ids. So the correct vertex object can be deduced by the 
	 * id of the this vertex and the direction of the adjacent vertex 
	 * relative to this vertex.
	 * 
	 * @param direction  "N","NE", etc, the position of the adjacent vertex 
	 * @param currentVertexId  the id of the vertex that currently being dealt with.
	 * @return the vertex object which is at the 
	 */
	private Vertex identifyAdjacentVertex(String direction, Graph graph) {
		
		int x = extractXAxisFromId();
		int y = extractYAxisFromId();
		int adjacent_x = x;
		int adjacent_y = y;
		
		switch(direction) {
			
			case "N": adjacent_y--; break; 
			case "NE": adjacent_y--; adjacent_x++; break;
			case "E": adjacent_x++; break;
			case "SE": adjacent_y++; adjacent_x++; break;
			case "S": adjacent_y++; break;
			case "SW": adjacent_y++; adjacent_x--; break;
			case "W": adjacent_x--; break;
			case "NW": adjacent_y--; adjacent_x--; break;
			default: throw new IllegalArgumentException("no such direction");
		
		}
							
		String adjacentId = makeToroidalId(adjacent_x, adjacent_y, graph);
		Vertex adjacentVertex = new Vertex();
		
		for (Vertex vertex : graph.getVertices()) {

			if (vertex.getId().equals(adjacentId)) {
				
				adjacentVertex = vertex;
				break;
				
			}
			
		}
		
		return adjacentVertex;
		
	}

	/**
	 * called from getAdjacentVertex(String, Graph)
	 * 
	 * If id string of vertex is 10x12, for example, then this method returns 
	 * an integer with the value 10 
	 * @return  the integer representing abscissa of this vertex
	 */
	private int extractXAxisFromId() {

		return Integer.parseInt(id.substring(0, id.indexOf('x')));
	
	}
	
	/**
	 * called from getAdjacentVertex(String, Graph)
	 * 
	 * If id string of vertex is 10x12, for example, then this method returns 
	 * an integer with the value 12 
	 * @return  the integer representing ordinate of this vertex
	 */
	private int extractYAxisFromId() {
	
		return Integer.parseInt(id.substring(id.indexOf('x')+1));
	
	}
	
	/**
	 * called from getAdjacentVertex(String, Graph)
	 * 
	 * @param pos_x
	 * @param pos_y
	 * @param graph
	 * @return
	 */
	private String makeToroidalId(int pos_x, int pos_y, Graph graph) {
				
		if (pos_x < 0) {
			
			pos_x = pos_x + graph.getDimensionsOfToroid();
			
		} else if (pos_x == graph.getDimensionsOfToroid()) {
			
			pos_x = 0;
		
		}
		
		if (pos_y < 0) {
			
			pos_y = pos_y + graph.getDimensionsOfToroid();
			
		} else if (pos_y == graph.getDimensionsOfToroid()) {
			
			pos_y = 0;
			
		}
		
		return "" +  pos_x + "x" + pos_y; 
	
	}
	
	@Override
	public String toString(){
		
		return "[" + id + "]";  
 		
	}
	
	@Override
	public boolean equals(Vertex vertex) {
		
		boolean result = false;
		
		if (vertex.getId() == this.getId() && 
			vertex.getNowState().equals(this.getNowState()) && 
			vertex.getNextState().equals(this.getNextState())) {
			
			result = true;
		
		}
		
		return result;
		
	}
	
	@Override
	public void setNextVertexInScanChain(Vertex vertex) {
		
		nextVertexInScanChain = vertex;
		
	}
	
	@Override
	public Vertex getNextVertexInScanChain() {
		
		return nextVertexInScanChain;
		
	}
	
	@Override
	public void setNowState(String state) {
		
		this.nowState = state;
		
	}
	
	@Override
	public void setNextState(String state) {

		this.nextState = state;
		
	}
	
	@Override
	public String getNowState() {
		
		return nowState;
		
	}

	@Override
	public String getNextState() {
		
		return nextState;
		
	}
	
	@Override
	public Map<String,Vertex> getNeighbourhood() {
		
		return neighbourhood;
		
	}

	@Override
	public String getNowType() {
		
		return nowType;
		
	}

	@Override
	public String getNextType() {
		
		return nextType;
		
	}
	
	@Override
	public void setNowType(String nowType) {
		
		this.nowType = nowType;
		
	}

	@Override
	public void setNextType(String nextType) {
		
		this.nextType = nextType;
		
	}
	
	@Override
	public void setNowHeadOfTRAM(boolean trueOrFalse) {
		
		nowHeadOfTRAM = trueOrFalse;
		
	}
	
	@Override
	public void setNextHeadOfTRAM(boolean trueOrFalse) {
		
		nextHeadOfTRAM = trueOrFalse;
		
	}
	
	@Override
	public boolean getNowHeadOfTRAM() {
		
		return nowHeadOfTRAM;
		
	}
	
	@Override
	public boolean getNextHeadOfTRAM() {
		
		return nextHeadOfTRAM;
		
	}
	
	@Override
	public void setNeighbourhood(Map<String, Vertex> neighbourhood) {
		
		this.neighbourhood = neighbourhood;
		
	}
	
	@Override
	public void set(String newId) {
		
		id = newId;
		
	}
	
	@Override
	public String getId() {
		
		return id;
		
	}

}