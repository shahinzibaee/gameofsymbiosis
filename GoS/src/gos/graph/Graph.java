package gos.graph;

import gos.GUI.GUI;
import gos.factories.GraphFactory;
import gos.interfaces.GraphIFC;

import java.util.List;
import java.util.Random;

public class Graph implements GraphIFC { 

	private List<Vertex> vertices;
	private int dimensionsOfToroid;
	private GUI gui;
	private GraphFactory graphFactory;
	private final static String NEWLINE = "\n";
	private final static String TAB = "\t";
	private int energyResources;
	private int energyBuffer;
	private Random random;
	public final static String START_VERTEX_ID = "0x0";
	private int numOfOnCells;
	private int cumulativeNumOfOnCells;
	private int timeSteps;
	
	/**
	 * Zero-args constructor
	 */
	public Graph() {
		
		graphFactory = new GraphFactory(this);
		
	}
	
	@Override
	public void buildGraph() {
		
		graphFactory.buildGraph();
	
	}
		
	@Override
	public Vertex getVertexWithId(String id) {
		
		Vertex vertexWithId = new Vertex();
		
		for (Vertex vertex: vertices) {

			if (vertex.getId().equals(id)) {
				
				vertexWithId = vertex;
				break;
				
			}
			
		}

		return vertexWithId;

	}
	
  	@Override
	public void makeNewRandomStates() {
		
		random = new Random();
		int onOrOff = 0;
		
		for (Vertex vertex : getVertices()) {
			
			onOrOff = random.nextInt(2);
			
			if (onOrOff == 0) {
				
				vertex.setNowState("off");
									
			} else if (onOrOff == 1) {
				
				vertex.setNowState("on");
				
			}
	
		}
			
	}
 	
	@Override
	public void updateCells() {
		
		gui.updateCellularAutomata();
		
	}
	
	@Override
	public void resetCounters() {
		
		numOfOnCells = 0;
		cumulativeNumOfOnCells = 0;
		timeSteps = 0;
		
	}

	/*represents a graph as a 2D grid. Vertices are shown according to Vertex.toString() */
	@Override
 	public String toString() {
		
		int counter = 1;
		String graphToString = "";

		try {
			
			if (vertices == null) {
				
				throw new NullPointerException("Vertices have not been initialised");
				
			}
		
			for (Vertex vertex : vertices) {
			
				graphToString += vertex.toString() + TAB;

				if (counter % dimensionsOfToroid == 0) {	
					
					graphToString += NEWLINE;	

				}	
			
				counter++;
			
			}		

		} catch (NullPointerException e) {
			
			System.out.println(e.getMessage());
			
		}
		
		return graphToString;
		
	}

	@Override
 	public String toString(String state) {
		
		int counter = 1;
		String graphToString = "";

		try {
			
			if (vertices == null) {
				
				throw new NullPointerException("Vertices have not been initialised");
				
			}
		
			for (Vertex vertex : vertices) {
			
				if (state.equals("nowStates")) {
					
					graphToString += convertOffOnStateTo01(vertex.getNowState());
				
				} else if (state.equals("nextStates")) {
					
					graphToString += convertOffOnStateTo01(vertex.getNextState());
				
				}
				
				if (counter % dimensionsOfToroid == 0) {	
					
					graphToString += NEWLINE;	

				}	
			
				counter++;
			
			}		

		} catch (NullPointerException e) {
			
			System.out.println(e.getMessage());
			
		}
		
		return graphToString;
		
	}
	
 	/**
  	 * called from toString(String)
  	 * takes a string "off" or "on" and returns "0" or "1"
 	 *  
 	 * @param state
 	 * @return
 	 */
 	private String convertOffOnStateTo01(String state) {
 		
 		String state0or1 = "";
 		
 		try {
 			
 			if (!state.equals("off") && !state.equals("on")) {
 				
 				throw new IllegalArgumentException("that state is neither off nor on");
 				
 			}
 			
 			if (state.equals("off")) {
 				
 				state0or1 = "0";
 				
 			} else {
 				
 				state0or1 = "1";
 				
 			}
 			
 		} catch (IllegalArgumentException e) {
 			
 			System.out.println(e.getMessage());

 		}
 		
 		return state0or1;
 		
 	}
 	
 
 	@Override
 	public int getNumOfOnCells() {
 		
 		return numOfOnCells;
 		
 	}
 	
	@Override
	public int getDimensionsOfToroid() {
		
		return dimensionsOfToroid;
		
	}	
	
 	@Override
	public void set(int dimensionsOfToroid) {
		
		this.dimensionsOfToroid = dimensionsOfToroid;
		
	}

 	@Override
	public void setEnergy(int resources, int buffer) {
		
 		setEnergyResources(resources);
		setEnergyBuffer(buffer);
		
	}
 	

	@Override
	public void setEnergyResources(int energyResources) {
		
		this.energyResources = energyResources;
		
	}
	
	@Override
	public void setEnergyBuffer(int buffer) {
		
		this.energyBuffer = buffer;
	
	}

 	@Override
	public int getEnergyResources() {
		
		return energyResources;
		
	}
 	
 	@Override
	public int getEnergyBuffer() {
		
		return energyBuffer;
		
	}
 	
 	
 	@Override
	public void set(GUI gui) {
		
		this.gui = gui;
		
	}
	
 	@Override
	public GraphFactory getGraphFactory() {
		
		return graphFactory;
		
	}

 	@Override
	public void setRulePanelToYellow() {
		
		gui.setRulePanelToYellow();
		
	}

 	@Override
	public void setRulePanelToRed() {
		
		gui.setRulePanelToRed();
		
	}
	
 	@Override
	public void setRulePanelToGreen() {
		
		gui.setRulePanelToGreen();
		
	}
 	
	@Override
	public void set(List<Vertex> vertices) {
		
		this.vertices = vertices;

	}
	
	@Override
	public List<Vertex> getVertices() {
		
		return vertices;
		
	}

	@Override
	public void setTimeSteps(int timeSteps) {
		
		this.timeSteps = timeSteps;
		
	}
	
	@Override
	public int getTimeSteps() {
		
		return timeSteps;
		
	}

	@Override
	public void set(int numOfOnCells, int cumulativeNumOfOnCells) {
		
		this.numOfOnCells = numOfOnCells;
		this.cumulativeNumOfOnCells = cumulativeNumOfOnCells;
		
	}
	
	@Override
	public int getCumulativeNumOfOnCells() {
		
		return cumulativeNumOfOnCells;
		
	}

	@Override
	public void incrementTimeSteps() {
		
		timeSteps++;
		
	}



/*	@Override
 	public void setNumOfOnCells(int numOfOnCells) {
 		
 		this.numOfOnCells = numOfOnCells;
 		
 	}*/
 	
	
		
/*	@Override
	public boolean equals(Object o) {
		
		boolean result = true;
		
		if (!o.getClass().equals(this.getClass())) {
			
			result = false;
			
		} 
		if (this.getVertices())

		return result;
		
	}*/
 	
}