package gos.visitors;

import java.util.Map;

import gos.graph.Graph;
import gos.graph.Vertex;
import gos.interfaces.UpdateStateVisitorIFC;

public class UpdateStateVisitor implements UpdateStateVisitorIFC {

	private Graph graph;
	private StateCollectionVisitor stateCollectionVisitor;
	private StateChangeVisitor stateChangeVisitor;
	private Map<Vertex,Integer> sumsOfNeighbourhoods; 
	private Map<Vertex,Integer> sumsOfNeumTRAMNeighbours;
		
	/**
	 * Constructor 
	 * 
	 * Assigns 'currentVertex' as the 0x0 vertex of the Graph, which is 
	 * the start vertex in the 'scan chain'.  
	 * 
	 * @param graph
	 */
	public UpdateStateVisitor(Graph graph) {
		
		set(graph);
		stateCollectionVisitor = new StateCollectionVisitor(graph);
		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
		sumsOfNeumTRAMNeighbours = stateCollectionVisitor.makeSumsOfNeumTRAMNeighbours();
		stateChangeVisitor = new StateChangeVisitor(graph);
		updateNextStates();//done once at instantiation to allow the first call of updateNowStates() from timertask to be able to set the new states.
		
	}
	
	@Override
	public void updateStatesAndCells() {
	
		updateNowStates();// might these be done in two or three different threads?
		updateNextStates();// might these be done in two or three different threads?
		graph.updateCells();// might these be done in two or three different threads?
				
	}
	
	@Override
	public void updateStatesTypesAndCells() {
		
		updateNowStatesTypes();
		updateNextStatesTypes();
		graph.updateCells();
				
	}
	
	/**
	 * Called from UpdateStatesAndCells(). 
	 * 
	 * Iterates scan chain of graph assigning nextState to nowState
	 * 
	 * Calls StateCollectionVisitor.makeSumsOfNeighbourhoods() 
	 * 
	 */
	private void updateNowStates() {		
		
		Vertex currentVertex = graph.getVertexWithId(Graph.START_VERTEX_ID);
		
		while (currentVertex != null) {
	
			currentVertex.setNowState(currentVertex.getNextState());
			currentVertex = currentVertex.getNextVertexInScanChain();
		
		}	

		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
	
	}
	
	/**
	 * Called from UpdateStatesTypesAndCells(). 
	 * Iterates scan chain of graph assigning nextState to nowState
	 * and nextType to nowType.
	 * 
	 * Calls StateCollectionVisitor.makeSumsOfNeighbourhoods() 
	 */
	private void updateNowStatesTypes() {		
		
		Vertex currentVertex = graph.getVertexWithId(Graph.START_VERTEX_ID);
		
		while (currentVertex != null) {
	
			currentVertex.setNowState(currentVertex.getNextState());
			currentVertex.setNowType(currentVertex.getNextType());
			currentVertex.setNowHeadOfTRAM(currentVertex.getNextHeadOfTRAM());
			currentVertex = currentVertex.getNextVertexInScanChain();
		
		}	

		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
		sumsOfNeumTRAMNeighbours = stateCollectionVisitor.makeSumsOfNeumTRAMNeighbours();
		
	}
	
	@Override
	public void updateNextStates() {
	
		stateChangeVisitor.updateNextStates(sumsOfNeighbourhoods);	
		
	}
	
	@Override
	public void updateNextStatesTypes() {
		
		stateChangeVisitor.updateNextStatesTypes(sumsOfNeighbourhoods,sumsOfNeumTRAMNeighbours);
		
	}
	
	@Override
	public void resetCumulativeOnCellsToZero() {
		
		stateCollectionVisitor.resetCumulativeOnCellsToZero();
		
	}


	@Override
	public void set(Graph graph) {
		
		this.graph = graph;
		
	}
	
	@Override
	public Graph getGraph() {
		
		return graph;
		
	}	
	
	@Override
	public StateCollectionVisitor getStateCollectionVisitor() {
		
		return stateCollectionVisitor;
		
	}

	@Override
	public StateChangeVisitor getStateChangeVisitor() {

		return stateChangeVisitor;

	}

	@Override
	public Map<Vertex, Integer> getSumsOfNeighbourhoods() {

		return sumsOfNeighbourhoods;
	
	}

	public void addTypesAndOneTRAM() {

		stateChangeVisitor.addTypesAndRandomTRAM();
		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
		sumsOfNeumTRAMNeighbours = stateCollectionVisitor.makeSumsOfNeumTRAMNeighbours();
		stateChangeVisitor.updateNextStatesTypes(sumsOfNeighbourhoods, sumsOfNeumTRAMNeighbours);
				
	}

}