package gos.visitors;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import gos.graph.Graph;
import gos.graph.Vertex;
import gos.interfaces.StateCollectionVisitorIFC;

/**
 * This visitor receives a Graph object and starts from its
 * "0x0" vertex. 
 * 
 * It iterates through the graph, one vertex at-a-time, 
 * using each vertex's own 'hard-wired' 'scan-chain' to find the
 * next vertex in the chain. 
 * 
 * At each vertex, it collects and sums up the number of adjacent vertices 
 * in the "on" state and stores all this in a hashmap with the vertex as 
 * the key and the sum of 'on' neighbours as the value.
 * 
 * @author Shahin
 * 
 */
public class StateCollectionVisitor implements StateCollectionVisitorIFC {
	
	private Graph graph;
	private int cumulativeNumOfOnCells;
	
	/**
	 * constructor
	 * @param startVertex
	 */
	public StateCollectionVisitor(Graph graph) {

		set(graph);		
				
	}
	
	@Override
	public Map<Vertex,Integer> makeSumsOfNeighbourhoods() {
		
		Map<Vertex,Integer> sumsOfNeighbourhoods = new HashMap<>();
		Vertex currentVertex = graph.getVertexWithId(Graph.START_VERTEX_ID);
		Integer sumOfOnNeighbours = 0;
		int numOfOnCells = 0; 
		
		while (currentVertex != null) {
		
			sumOfOnNeighbours = calcSumOfOnNeighboursFor(currentVertex);
			sumsOfNeighbourhoods.put(currentVertex, sumOfOnNeighbours);
			numOfOnCells = addUpOnCells(currentVertex, numOfOnCells);
			currentVertex = currentVertex.getNextVertexInScanChain();
			
		}
		
		graph.set(numOfOnCells,cumulativeNumOfOnCells);
		return sumsOfNeighbourhoods;

	}
	
	/**
	 * Called from makeSumsOfNeighbourhoods()
	 *  
	 * @param vertex
	 * @return sum of neighbours in 'on' state
	 */
	private Integer calcSumOfOnNeighboursFor(Vertex currentVertex) {
		
		Integer sumOfOnNeighbours = 0 ;
		
		for (Vertex adjacentVertex : currentVertex.getNeighbourhood().values()) {

			if (adjacentVertex.getNowState().equals("on")) {
				
				sumOfOnNeighbours++;
				
			}
			
		}
		
		return sumOfOnNeighbours;
		
	}
	
	/**
	 * called from makeSumsOfNeighbourhoods()
	 *  
	 * @param currentVertex
	 */
	private int addUpOnCells(Vertex currentVertex, int numOfOnCells) {
			
		if (currentVertex.getNowState().equals("on")) {
				
				numOfOnCells++;
				cumulativeNumOfOnCells++;
			
		}
		
		return numOfOnCells;

	}
	
	@Override
	public int getCumulativeNumOfOnCells() {
		
		return cumulativeNumOfOnCells;
		
	}

	/**
	 * called from the constructor of UpdateStateVisitor
	 * 
	 * @param 
	 * @return
	 */
	public Map<Vertex,Integer> makeSumsOfNeumTRAMNeighbours() {
		
		Map<Vertex,Integer> sumsOfNeumannTRAMNeighbours = new HashMap<>();
		Vertex currentVertex = graph.getVertexWithId(Graph.START_VERTEX_ID);

		while (currentVertex != null) {
		
			sumsOfNeumannTRAMNeighbours.put(currentVertex, calcSumOfTRAMNeighboursFor(currentVertex));
			currentVertex = currentVertex.getNextVertexInScanChain();
			
		}
		
		return sumsOfNeumannTRAMNeighbours;
	}
	
	/**
	 * 
	 * Called from makeSumsOfNeumannTRAMNeighbours()
	 * 
	 * calculates number of TRAMs in currentVertex's von Neumann's Neighbourhood
	 *  
	 * @param vertex
	 * @return sum of neighbours in 'on' state
	 */
	private Integer calcSumOfTRAMNeighboursFor(Vertex currentVertex) {
		
		Integer sumOfTRAMNeighbours = 0 ;
				
		for (Entry<String,Vertex> directionAdjacentVertex : makeNeumNeighoodFor(currentVertex).entrySet()) {

			if (directionAdjacentVertex.getValue().getNowType().equals("TRAM")) {
				
				sumOfTRAMNeighbours++;
				
			}
			
		}
		
		return sumOfTRAMNeighbours;
	
	}
	
	/**
	 * called from calculateNextHeadOfTRAM()
	 * Called from UpdateStateVisitor.updateNowStates().
	 * 
	 * constructs a map of N,S,E,W adjacent vertices for current Vertex.
	 * uses the MooresNeighbourhood map called neighbourhood which is 
	 * stored in every vertex. From this it extracts just the 4 in von 
	 * Neumann's neighbourhood and puts them in the new hashmap 
	 * (which is only passed and used between methods, not stored as a field)
	 * 
	 * @param currentVertex
	 * @return vonNeumannNeighbourhood with direction string. E.g. "N" paired with the adjacent vertex at position "N"
	 */
	private Map<String,Vertex> makeNeumNeighoodFor(Vertex currentVertex) {
		
		Map<String,Vertex> vonNeumannNeighbourhood = new HashMap<>();
				
		for (Entry<String,Vertex> entrySet : currentVertex.getNeighbourhood().entrySet()) {
			
			addToNeumNeighood(vonNeumannNeighbourhood, entrySet.getKey(),entrySet.getValue());
			
		}
		
		return vonNeumannNeighbourhood;
				
	}
	
	/**
	 * called from makeVonNeumannNeighbourhood(Vertex)
	 * 
	 * @param vonNeumannNeighbourhood
	 * @param direction
	 * @param vertex
	 */
	private void addToNeumNeighood(Map<String,Vertex> neumNeighood, String direction, Vertex vertex) {
	
		switch (direction) {
		
			case "N" : neumNeighood.put("N",vertex); break;
			case "S" : neumNeighood.put("S",vertex); break;
			case "E" : neumNeighood.put("E",vertex); break;
			case "W" : neumNeighood.put("W",vertex); break;
			default  :  break;
			
		}
	
	}
	
	@Override
	public void resetCumulativeOnCellsToZero() {
		
		cumulativeNumOfOnCells = 0;
		
	}
	
	/**
	 * called from constructor
	 * setter 
	 * @param currentVertex
	 */
	public void set(Graph graph) {
		
		this.graph = graph;
		
	}
	
	/**
	 * for JUnit
	 * @return
	 */
	public Graph getGraph() {
		
		return graph;
		
	}

}