package gos.GUI;

import gos.factories.ButtonFactory;
import gos.factories.CellularAutomataFactory;
import gos.factories.WindowFactory;
import gos.graph.Graph;
import gos.interfaces.GuiIFC;
import gos.main.UserInput;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame implements GuiIFC {
	
	private static final long serialVersionUID = 5951201219124589120L;
	private JPanel mainWindow;
	private 	ButtonFactory buttonFactory;
	private WindowFactory windowFactory;
	private CellularAutomataFactory cellularAutomataFactory;
	private Graph graph;

	public GUI() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		windowFactory = new WindowFactory();
		mainWindow = windowFactory.makeMainWindow(this);
		windowFactory.makeRulePanel(mainWindow);
		buttonFactory = new ButtonFactory();
		buttonFactory.makeButtonsAndInputs(mainWindow);
		cellularAutomataFactory = new CellularAutomataFactory();
		mainWindow.add(windowFactory.makeUserControlPanel());

	}
	
	@Override
	public void updateCellularAutomata() {

		cellularAutomataFactory.updateCellsToUpdated(graph);
		buttonFactory.set(graph.getNumOfOnCells(), graph.getCumulativeNumOfOnCells(), graph.getTimeSteps(), graph.getEnergyResources(), graph.getEnergyBuffer());
				
	}
	
	@Override
	public void makeCellularAutomata() {
		
		cellularAutomataFactory.makeCellularAutomata(graph, mainWindow);
		
	}
	
	@Override
	public void set(Graph graph, UserInput userInput) {
		
		this.graph = graph;
		graph.set(this);
		buttonFactory.set(userInput);
		
	}
	
	@Override
	public void set(Graph graph) {
		
		this.graph = graph;
	
		
	}
	
	@Override
	public Graph getGraph() {

		return graph;
		
	}

	@Override
	public void setRulePanelToYellow() {
	
		windowFactory.setRulePanelToYellow();
		
	}
	
	@Override
	public void setRulePanelToRed() {
		
		windowFactory.setRulePanelToRed();
		
	}

	@Override
	public void setRulePanelToGreen() {
		
		windowFactory.setRulePanelToGreen();
		
	}

	@Override
	public JPanel getMainWindow() {
		
		return mainWindow;
		
	}

	public void resetToDefaultDimensionsOfToroid() {
		
		buttonFactory.resetToDefaultDimensionsOfToroid();
		
	}
	
	public void resetToDefaultUpdatesPerSec() {
		
		buttonFactory.resetToDefaultUpdatesPerSec();
	
	}
	
}