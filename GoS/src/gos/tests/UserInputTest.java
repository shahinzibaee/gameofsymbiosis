package gos.tests;

import static org.junit.Assert.*;
import gos.factories.GraphFactory;
import gos.graph.Graph;
import gos.main.InputOutput;
import gos.main.Simulation;
import gos.main.UserInput;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserInputTest {

	private Graph graph;
	private GraphFactory graphFactory;
	private UserInput userInput;
	private Simulation simulation;
	private InputOutput io;
	
	@Before
	public void setUp() throws Exception {
	
		graph = new Graph();
		graphFactory = new GraphFactory(graph);
		graphFactory.setDefaultDimensionsOfToroid();
		graphFactory.buildGraph();
		simulation = new Simulation(graph);
		io = new InputOutput();
		userInput = new UserInput(graph, io, simulation);
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testUserInput() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetDefaultNumOfVertices() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetResponseToButtonClick() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetDefaultInputs() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetNewRandomStates() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetDimensionOfToroidInGraphFactory() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetTimeStepInSimulation() {
		fail("Not yet implemented");
	}

	@Test
	public void testStartSimulation() {
		fail("Not yet implemented");
	}
	
}
