package gos.tests;

import static org.junit.Assert.*;

import gos.factories.GraphFactory;
import gos.graph.Graph;
import gos.graph.Vertex;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GraphTest {

	Graph graph;
	GraphFactory graphFactory;
	
	@Before
	public void setUp() throws Exception {
		
		int dimensionsOfToroid = 2;
		graph = new Graph();
		graphFactory = new GraphFactory();
		graphFactory.set(graph);
		graphFactory.set(dimensionsOfToroid);
		graphFactory.makeVerticesForGraph();

	}

	@After
	public void tearDown() throws Exception {
	
		graph = null;
		graphFactory = null;
		
	}

	@Test
	public void testGraph_Int() {

		int dimensionsOfToroid = 6;
		graph = new Graph();//testing this constructor
		graph.set(dimensionsOfToroid);
		int actualValue = graph.getDimensionsOfToroid();
		int expectedValue = 6;
		
		assertEquals(expectedValue, actualValue);
		
	}

	@Test
	public void testSetVertices() {
		
		assertNotNull(graph.getVertices());//testing method called by this method

	}
	
	@Test
	public void testGetVertexWithId() {
		
		Vertex actualVertex = graph.getVertexWithId("1x1");//testing this method
		String actualValue = actualVertex.getId();
		String expectedValue = "1x1";
		
		assertEquals(expectedValue, actualValue);
		
	}

	@Test
	public void testGetVertices() {

		assertNotNull(graph.getVertices());//testing this method
		
	}

	@Test
	public void testGetDimensionOfToroid() {
	
		int expectedValue = 2;
		int actualValue = graph.getDimensionsOfToroid();//testing this method
	
		assertEquals(expectedValue, actualValue);
	
	}
	
	@Test
	public void testEquals() {
		fail("Not yet implemented");
	}

	@Test
	public void testToString() {

		String expectedValue = "[0x0]\t[1x0]\t\n[0x1]\t[1x1]\t\n";
		String actualValue = graph.toString();//testing this method
	
		assertEquals(expectedValue, actualValue);

	}

	@Test
	public void testSetRandomStates() {
		
		boolean onOrOff = true;
		
		for (Vertex vertex : graph.getVertices()) {
			
			assertNull(vertex.getNextState());
			assertNotNull(vertex.getNowState());
			
		}
		
/*		graph.setNewRandomStates();//method being tested

		for (Vertex vertex : graph.getVertices()) {
		
			assertNull(vertex.getNextState());
			assertNotNull(vertex.getNowState());
		
			if (!vertex.getNowState().equals("on")) {
				
				if (!vertex.getNowState().equals("off")) {

					onOrOff = false;

				}
				
				
			} 
						
		}
*/		
		assertTrue(onOrOff);
		
	}
	
}