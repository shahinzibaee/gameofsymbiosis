package gos.tests;

import static org.junit.Assert.*;


import gos.factories.GraphFactory;
import gos.graph.Graph;
import gos.main.Simulation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SimulationTest { // need to complete writing the testUpdate()

	private Graph graph;
	private GraphFactory graphFactory;
	private final int dimensionsOfToroid = 4;
	private Simulation simulation;
	
	@Before
	public void setUp() throws Exception {
	
		graph = new Graph();
		graphFactory = new GraphFactory(graph);
		graphFactory.set(dimensionsOfToroid);
		simulation = new Simulation(graph);
		
		graph.getVertexWithId("0x0").setNowState("on");
		graph.getVertexWithId("0x1").setNowState("on");
		graph.getVertexWithId("1x1").setNowState("off");
		graph.getVertexWithId("1x0").setNowState("off");
// need to set the states for all the vertices in this 4x4 graph
// and then to calculate what the next states should be.
		
	}

	@After
	public void tearDown() throws Exception {
	
		graph = null;
		graphFactory = null;
		simulation = null;
		
	}

	/**
	 * Called by startUpdatingGoS().
	 * Calls three methods in succession: 
	 * 1. UpdateStateVisitor.updateNowStates()
	 * 2. GuiFactory.updateCells()
	 * 3. UpdateStateVisitor.updateNextStates()
	 */
	@Test
	public void testUpdate() {
		
		simulation.update();
		
		
	}

	@Test
	public void testStartUpdatingGoS() {
		fail("Not yet implemented");
	}


}
