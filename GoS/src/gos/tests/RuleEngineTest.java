package gos.tests;

import static org.junit.Assert.*;
import gos.ruleEngine.RuleEngine;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RuleEngineTest {

	RuleEngine ruleEngine;
	
	@Before
	public void setUp() throws Exception {
	
		ruleEngine = new RuleEngine();
		
	}

	@After
	public void tearDown() throws Exception {
	
		ruleEngine = null;
		
	}

	@Test
	public void testCalculateNextStatesWithGoLRules_String_int() {

		String actualValue0 = ruleEngine.calculateNextStateWithGoLRules("on",0);
		String expectedValue0 = "off";
		assertEquals(expectedValue0, actualValue0);

		String actualValue1 = ruleEngine.calculateNextStateWithGoLRules("on",1);
		String expectedValue1 = "off";
		assertEquals(expectedValue1, actualValue1);

		String actualValue2 = ruleEngine.calculateNextStateWithGoLRules("on",2);
		String expectedValue2 = "on";
		assertEquals(expectedValue2, actualValue2);

		String actualValue3 = ruleEngine.calculateNextStateWithGoLRules("on",3);
		String expectedValue3 = "on";
		assertEquals(expectedValue3, actualValue3);

	
		String actualValue4 = ruleEngine.calculateNextStateWithGoLRules("on",4);
		String expectedValue4 = "off";
		assertEquals(expectedValue4, actualValue4);

		String actualValue5 = ruleEngine.calculateNextStateWithGoLRules("on",5);
		String expectedValue5 = "off";
		assertEquals(expectedValue5, actualValue5);

		String actualValue6 = ruleEngine.calculateNextStateWithGoLRules("off",0);
		String expectedValue6 = "off";
		assertEquals(expectedValue6, actualValue6);

		String actualValue7 = ruleEngine.calculateNextStateWithGoLRules("off",1);
		String expectedValue7 = "off";
		assertEquals(expectedValue7, actualValue7);

		String actualValue8 = ruleEngine.calculateNextStateWithGoLRules("off",2);
		String expectedValue8 = "off";
		assertEquals(expectedValue8, actualValue8);

		String actualValue9 = ruleEngine.calculateNextStateWithGoLRules("off",3);
		String expectedValue9 = "on";
		assertEquals(expectedValue9, actualValue9);

	}

}