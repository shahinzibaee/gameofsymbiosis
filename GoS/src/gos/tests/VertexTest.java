package gos.tests;

import static org.junit.Assert.*;
import gos.factories.GraphFactory;
import gos.graph.Graph;
import gos.graph.Vertex;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VertexTest {

	Graph graph;
	GraphFactory graphFactory;
	Vertex vertex;

	@Before
	public void setUp() throws Exception {
		
		int dimensionsOfToroid = 5;
		graph = new Graph();
		graphFactory = new GraphFactory();
		graphFactory.set(graph);
		graphFactory.set(dimensionsOfToroid);
		graphFactory.makeVerticesForGraph();
		graphFactory.initDirection8Pairs();
		vertex = new Vertex();

	}

	@After
	public void tearDown() throws Exception {
		
		vertex = null;
		graph = null;
		graphFactory = null;
	
	}

	/** 
	 * If attach(String,Graph) works correctly, it should show that it
	 * has: 
	 * (1) attached something to the specified position, i.e. should 
	 * be not null at the position. 
	 * (2) attached the correct vertex, as defined by their ids being 
	 * correct for their relative positions. 
	 * (3) attached the adjacent vertex back reciprocally to this vertex
	 * (in the opposite 'direction').
	 * 	 
	 */
	 @Test
	public void testAttachNeighbourhood() {

		vertex.set("0x0");
		vertex.attachNeighbourhood("NW,SE", graph);//testing this method
		Vertex adjacentVertexActual = vertex.identifyAdjacentVertex("NW", graph);//should be the correct vertex if attach() has worked
		String actualValue = adjacentVertexActual.getId();
		Vertex adjacentVertexActual2 = vertex.identifyAdjacentVertex("SE", graph);//should also have correct vertex if attach() did its job
		String actualValue2 = adjacentVertexActual2.getId();
		Vertex adjacentVertexExpected = new Vertex();
		adjacentVertexExpected.set("4x4");//northwest of 0x0
		String expectedValue = adjacentVertexExpected.getId();
		Vertex adjacentVertexExpected2 = new Vertex();
		adjacentVertexExpected2.set("1x1");//southeast of 0x0
		String expectedValue2 = adjacentVertexExpected2.getId();

		assertEquals(expectedValue, actualValue);
		assertEquals(expectedValue2, actualValue2);
		
	}

	@Test
	public void testGetXNumFromId() {
		
		Vertex vertex = graph.getVertexWithId("0x0");
		int actualValue = vertex.extractXAxisFromId();
		int expectedValue = 0;
		
		vertex.set("10x12");
		int actualValue2 = vertex.extractXAxisFromId();
		int expectedValue2 = 10;

		vertex.set("1045x124442");
		int actualValue3 = vertex.extractXAxisFromId();
		int expectedValue3 = 1045;

		assertEquals(expectedValue, actualValue);
		assertEquals(expectedValue2, actualValue2);
		assertEquals(expectedValue3, actualValue3);

		
	}
	
	@Test
	public void testGetYNumFromId() {
		
		Vertex vertex = graph.getVertexWithId("0x0");
		int actualValue = vertex.extractYAxisFromId();
		int expectedValue = 0;
		
		vertex.set("10x12");
		int actualValue2 = vertex.extractYAxisFromId();
		int expectedValue2 = 12;
	
		vertex.set("1045x124442");
		int actualValue3 = vertex.extractYAxisFromId();
		int expectedValue3 = 124442;
		
		assertEquals(expectedValue, actualValue);
		assertEquals(expectedValue2, actualValue2);
		assertEquals(expectedValue3, actualValue3);

	}

	
	@Test
	public void testGetAdjacentVertex() {
	
		vertex.set("0x0");
		Vertex vertexActual = vertex.identifyAdjacentVertex("N", graph);//testing this method
		String actualValue = vertexActual.getId();
		Vertex vertexExpected = new Vertex();
		vertexExpected.set("0x4");
		String expectedValue = vertexExpected.getId();
		
		assertEquals(expectedValue, actualValue);
		
	}

	@Test
	public void testGetNeighbourhood() {

		Vertex vertex = graph.getVertexWithId("0x0");
		System.out.println(vertex.getNeighbourhood());
		graphFactory.attachAllNeighbourhoods();
		System.out.println(vertex.getNeighbourhood());
	
	}
	
	@Test
	public void testMakeToroidalId() {
		
		vertex.set("0x0");
		String actualValue = vertex.makeToroidalId(-1, 5, graph);//testing this method
		String expectedValue = "4x0";
		
		assertEquals(expectedValue, actualValue);
	
		String actualValue2 = vertex.makeToroidalId(4, 2, graph);//testing this method
		String expectedValue2 = "4x2";
		
		assertEquals(expectedValue2, actualValue2);

		String actualValue3 = vertex.makeToroidalId(5, 0, graph);
		String expectedValue3 = "0x0";
		
		assertEquals(expectedValue3, actualValue3);
	
	}

	@Test
	public void testEquals() {
		//todo;
	}

	@Test
	public void testToString() {
		
		vertex.set("0x0");
		String actualValue = vertex.toString();//testing this method
		String expectedValue = "[0x0]";
		
		assertEquals(expectedValue, actualValue);
		
	}

}