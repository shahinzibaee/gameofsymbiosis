package gos.tests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import gos.factories.GraphFactory;
import gos.graph.Graph;
import gos.graph.Vertex;
import gos.visitors.StateChangeVisitor;
import gos.visitors.StateCollectionVisitor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StateChangeVisitorTest {

	private Graph graph;
	private GraphFactory graphFactory;
	private StateCollectionVisitor stateCollectionVisitor;
	private StateChangeVisitor stateChangeVisitor;
	private final String idOfStartVertex = "0x0";
	private Vertex currentVertex;
	private Map<String,Vertex> neighbourhood;
	private int dimensionsOfToroid = 8;
	private Map<String,Vertex> vonNeumannNeighbourhood; 
	private Map<Vertex, Integer> sumsOfNeighbourhoods;
	private Map<Vertex, Integer> sumsOfNeumannTRAMNeighbours;
	
	@Before
	public void setUp() throws Exception {
	
		graph = new Graph();
		graphFactory = new GraphFactory(graph);
		graphFactory.set(dimensionsOfToroid);
		graphFactory.buildGraph();
		currentVertex = graph.getVertexWithId(idOfStartVertex);
		stateCollectionVisitor = new StateCollectionVisitor(graph);
		stateChangeVisitor = new StateChangeVisitor(graph);
		vonNeumannNeighbourhood = new HashMap<>();
		
		//just to be clear about the exact pairings in the neighbourhood map.
		neighbourhood = new HashMap<>();
		//neighbourhood of vertex 0x0
		int WestOrNorthOf0x0 = dimensionsOfToroid - 1;
		neighbourhood.put("N",graph.getVertexWithId("0x"+WestOrNorthOf0x0));
		neighbourhood.put("NE",graph.getVertexWithId("1x"+WestOrNorthOf0x0));
		neighbourhood.put("E",graph.getVertexWithId("1x0"));
		neighbourhood.put("SE",graph.getVertexWithId("1x1"));
		neighbourhood.put("S",graph.getVertexWithId("0x1"));
		neighbourhood.put("SW",graph.getVertexWithId(""+WestOrNorthOf0x0+"x1"));
		neighbourhood.put("W",graph.getVertexWithId(WestOrNorthOf0x0+"x0"));
		neighbourhood.put("NW",graph.getVertexWithId(""+WestOrNorthOf0x0+"x"+WestOrNorthOf0x0));
		
		graph.getVertexWithId("0x0").setNowState("on");
		graph.getVertexWithId("0x7").setNowState("off");
		graph.getVertexWithId("1x7").setNowState("off");
		graph.getVertexWithId("1x0").setNowState("off");
		graph.getVertexWithId("1x1").setNowState("off");
		graph.getVertexWithId("0x1").setNowState("on");
		graph.getVertexWithId("7x1").setNowState("on");
		graph.getVertexWithId("7x0").setNowState("off");
		graph.getVertexWithId("7x7").setNowState("off");
		currentVertex.setNeighbourhood(neighbourhood);		

	}

	@After
	public void tearDown() throws Exception {
	
		graph = null;
		graphFactory = null;
		currentVertex = null;
		stateCollectionVisitor = null;
		stateChangeVisitor = null;
		neighbourhood = null;
		
	}
	/**
	 * Is called by setNextStates(). 
	 * 
	 * Calls RuleEngine.calculateNextStatesWithGoLRules(String, int), which 
	 * has been JUnit tested and passes.
	 * 
	 */
	@Test
	public void testCalculateNextStateOfCurrentVertex() {

		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
		String actualValue = stateChangeVisitor.calculateNextStateOf(currentVertex, sumsOfNeighbourhoods);
		String expectedValue = "on";
		assertEquals(expectedValue, actualValue);
		
//		graph.setEnergy(10);
//		stateChangeVisitor.setNumOfOnCells(9);
		
		
	}
	
	/**
	 * Calls Vertex.setNextState(String), which has already passed JUnit test.
	 * 
	 * Calls calculateNextStateOfCurrentVertex(Vertex,Map<Vertex,Integer>), 
	 * which has passed JUnit test.
	 * 
	 */
	@Test
	public void testUpdateNextStates() {

		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
		stateChangeVisitor.updateNextStates(sumsOfNeighbourhoods);
		String actualState = graph.getVertexWithId("0x0").getNextState();
//0x0 is "on" and has 2 "on" neighbours, therefore next state should by GoL rules remain "on"
		String expectedState = "on";
		assertEquals(expectedState,actualState);
		
		graph.getVertexWithId("0x0").setNowState("on");//currentVertex
		graph.getVertexWithId("0x7").setNowState("off");
		graph.getVertexWithId("1x7").setNowState("off");
		graph.getVertexWithId("1x0").setNowState("off");
		graph.getVertexWithId("1x1").setNowState("off");
		graph.getVertexWithId("0x1").setNowState("off");
		graph.getVertexWithId("7x1").setNowState("on");//1 on neighbour
		graph.getVertexWithId("7x0").setNowState("off");
		graph.getVertexWithId("7x7").setNowState("off");
		currentVertex.setNeighbourhood(neighbourhood);		
		stateCollectionVisitor.calculateSumOfOnNeighboursFor(currentVertex);
		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
		sumsOfNeumannTRAMNeighbours = stateCollectionVisitor.makeSumsOfNeumannTRAMNeighbours();
		stateChangeVisitor.updateNextStates(sumsOfNeighbourhoods);
		String expectedState2 = "off";
		String actualState2 = graph.getVertexWithId("0x0").getNextState();
		assertEquals(expectedState2,actualState2);
		
		graph.getVertexWithId("0x0").setNowState("on");//currentVertex
		graph.getVertexWithId("0x7").setNowState("off");
		graph.getVertexWithId("1x7").setNowState("off");
		graph.getVertexWithId("1x0").setNowState("off");
		graph.getVertexWithId("1x1").setNowState("off");
		graph.getVertexWithId("0x1").setNowState("on");//1 on neighbour 
		graph.getVertexWithId("7x1").setNowState("on");//2 on neighbours
		graph.getVertexWithId("7x0").setNowState("on");//3 on neighbours
		graph.getVertexWithId("7x7").setNowState("off");
		currentVertex.setNeighbourhood(neighbourhood);		
		stateCollectionVisitor.calculateSumOfOnNeighboursFor(currentVertex);
		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
		stateChangeVisitor.updateNextStates(sumsOfNeighbourhoods);
		String expectedState3 = "on";
		String actualState3 = graph.getVertexWithId("0x0").getNextState();
		assertEquals(expectedState3,actualState3);
		
		graph.getVertexWithId("0x0").setNowState("on");//current vertex
		graph.getVertexWithId("0x7").setNowState("on");//1 on neighbour
		graph.getVertexWithId("1x7").setNowState("on");//2 on neighbours
		graph.getVertexWithId("1x0").setNowState("off");
		graph.getVertexWithId("1x1").setNowState("off");
		graph.getVertexWithId("0x1").setNowState("on");//3 on neighbours
		graph.getVertexWithId("7x1").setNowState("on");//4 on neighbours
		graph.getVertexWithId("7x0").setNowState("off");
		graph.getVertexWithId("7x7").setNowState("off");
		currentVertex.setNeighbourhood(neighbourhood);		
		stateCollectionVisitor.calculateSumOfOnNeighboursFor(currentVertex);
		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
		stateChangeVisitor.updateNextStates(sumsOfNeighbourhoods);
		String expectedState4 = "off";
		String actualState4 = graph.getVertexWithId("0x0").getNextState();
		assertEquals(expectedState4, actualState4);
	
		graph.getVertexWithId("0x0").setNowState("off");//current vertex
		graph.getVertexWithId("0x7").setNowState("on");//1 on neighbour
		graph.getVertexWithId("1x7").setNowState("on");//2 on neighbours
		graph.getVertexWithId("1x0").setNowState("off");
		graph.getVertexWithId("1x1").setNowState("off");
		graph.getVertexWithId("0x1").setNowState("on");//3 on neighbours
		graph.getVertexWithId("7x1").setNowState("off");
		graph.getVertexWithId("7x0").setNowState("off");
		graph.getVertexWithId("7x7").setNowState("off");
		currentVertex.setNeighbourhood(neighbourhood);		
		stateCollectionVisitor.calculateSumOfOnNeighboursFor(currentVertex);
		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
		stateChangeVisitor.updateNextStates(sumsOfNeighbourhoods);
		String expectedState5 = "on";
		String actualState5 = graph.getVertexWithId("0x0").getNextState();
		assertEquals(expectedState5, actualState5);
		
		graph.getVertexWithId("0x0").setNowState("off");//current vertex
		graph.getVertexWithId("0x7").setNowState("on");//1 on neighbour
		graph.getVertexWithId("1x7").setNowState("off");
		graph.getVertexWithId("1x0").setNowState("off");
		graph.getVertexWithId("1x1").setNowState("off");
		graph.getVertexWithId("0x1").setNowState("off");
		graph.getVertexWithId("7x1").setNowState("off");
		graph.getVertexWithId("7x0").setNowState("off");
		graph.getVertexWithId("7x7").setNowState("off");
		currentVertex.setNeighbourhood(neighbourhood);		
		stateCollectionVisitor.calculateSumOfOnNeighboursFor(currentVertex);
		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
		stateChangeVisitor.updateNextStates(sumsOfNeighbourhoods);
		String expectedState6 = "off";
		String actualState6 = graph.getVertexWithId("0x0").getNextState();
		assertEquals(expectedState6, actualState6);		

		graph.getVertexWithId("0x0").setNowState("off");//current vertex
		graph.getVertexWithId("0x7").setNowState("on");//1 on neighbour
		graph.getVertexWithId("1x7").setNowState("on");//2 on neighbours
		graph.getVertexWithId("1x0").setNowState("off");
		graph.getVertexWithId("1x1").setNowState("off");
		graph.getVertexWithId("0x1").setNowState("off");
		graph.getVertexWithId("7x1").setNowState("off");
		graph.getVertexWithId("7x0").setNowState("off");
		graph.getVertexWithId("7x7").setNowState("off");
		currentVertex.setNeighbourhood(neighbourhood);		
		stateCollectionVisitor.calculateSumOfOnNeighboursFor(currentVertex);
		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
		stateChangeVisitor.updateNextStates(sumsOfNeighbourhoods);
		String expectedState7 = "off";
		String actualState7 = graph.getVertexWithId("0x0").getNextState();
		assertEquals(expectedState7, actualState7);		

		graph.getVertexWithId("0x0").setNowState("off");//current vertex
		graph.getVertexWithId("0x7").setNowState("on");//1 on neighbour
		graph.getVertexWithId("1x7").setNowState("on");//2 on neighbours
		graph.getVertexWithId("1x0").setNowState("off");
		graph.getVertexWithId("1x1").setNowState("off");
		graph.getVertexWithId("0x1").setNowState("on");//3 on neighbours
		graph.getVertexWithId("7x1").setNowState("off");
		graph.getVertexWithId("7x0").setNowState("on");//4 on neighbours
		graph.getVertexWithId("7x7").setNowState("off");
		currentVertex.setNeighbourhood(neighbourhood);		
		stateCollectionVisitor.calculateSumOfOnNeighboursFor(currentVertex);
		sumsOfNeighbourhoods = stateCollectionVisitor.makeSumsOfNeighbourhoods();
		stateChangeVisitor.updateNextStates(sumsOfNeighbourhoods);
		String expectedState8 = "off";
		String actualState8 = graph.getVertexWithId("0x0").getNextState();
		assertEquals(expectedState8, actualState8);
		
	}

	
	/**
	 * moved code to StateCollectionVisitor
	 * "XxY" - hence north/south changes Y, east/west changes X
	 */
	@Test
	public void testMakeVonNeumannNeighbourhood() {
		
		Map<String, Vertex> vonNeumannNeighbourhood = stateCollectionVisitor.makeVonNeumannNeighbourhoodFor(currentVertex);
		//neighbourhood of vertex 0x0
		String actualIdOfN = vonNeumannNeighbourhood.get("N").getId();
		String expectedIdOfN = "0x7";//cos dimensionsOfToroid is 8
		assertEquals(expectedIdOfN,actualIdOfN);
		assertNull(vonNeumannNeighbourhood.get("NE"));
		String actualIdOfE = vonNeumannNeighbourhood.get("E").getId();
		String expectedIdOfE = "1x0";
		assertEquals(expectedIdOfE,actualIdOfE);
		assertNull(vonNeumannNeighbourhood.get("SE"));
		String actualIdOfS = vonNeumannNeighbourhood.get("S").getId();
		String expectedIdOfS = "0x1";
		assertEquals(expectedIdOfS,actualIdOfS);
		assertNull(vonNeumannNeighbourhood.get("SW"));	
		String actualIdOfW = vonNeumannNeighbourhood.get("W").getId();
		String expectedIdOfW = "7x0";
		assertEquals(expectedIdOfW,actualIdOfW);
		assertNull(vonNeumannNeighbourhood.get("NW"));

		int expectedSizeOfMooresNeighbourhood = 8;
		int actualSizeOfMooresNeighbourhood = neighbourhood.size();
		assertEquals(expectedSizeOfMooresNeighbourhood, actualSizeOfMooresNeighbourhood);
		
		int expectedSizeOfvonNeumannNeighbourhood = 4;
		int actualSizeOfvonNeumannNeighbourhood = vonNeumannNeighbourhood.size();
		assertEquals(expectedSizeOfvonNeumannNeighbourhood, actualSizeOfvonNeumannNeighbourhood);
		
	}
	
	@Test
	public void testAddToVonNeumann() {
		//todo
	}
	
	
	/**
	 * calls isNextHeadOfTRAMFor(Vertex)
	 * calls isTailOfTRAMFor(Vertex)
	 * 
	 */
	@Test
	public void testCalculateNextTypeOf() {
		
		stateChangeVisitor.calculateNextTypeOf(currentVertex, sumsOfNeumannTRAMNeighbours);
		
	}

}