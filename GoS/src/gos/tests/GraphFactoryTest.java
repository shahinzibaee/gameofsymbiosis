package gos.tests;

import static org.junit.Assert.*;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import gos.factories.GraphFactory;
import gos.graph.Graph;
import gos.graph.Vertex;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GraphFactoryTest {

	private GraphFactory graphFactory;
	private Graph graph;
	private Vertex vertex;
	private Vertex vertex2;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		
		graph = null;
		graphFactory = null;
		vertex = null;
		
	}

	/**
	 * test that constructor which takes a graph object
	 * argument, gets the 'dimensionOfToroid' from the graph
	 * and assigns it to its own dimensionOfToroid member
	 * field. 
	 */
	@Test
	public void testGraphFactory_Graph() {
		
		int dimensionsOfToroid = 16;
		graph = new Graph();
		graphFactory = new GraphFactory(graph);//testing this constructor
		graph.set(dimensionsOfToroid);
		int expectedValue = dimensionsOfToroid; 
		int actualValue = graph.getDimensionsOfToroid();
		
		assertEquals(expectedValue, actualValue);
		
	}

	/**
	 *
	 */
	@Test
	public void testMakeVerticesFor_Graph() {
		
		int dimensionsOfToroid = 4;
		graph = new Graph();
		graph.set(dimensionsOfToroid);
		graphFactory = new GraphFactory();
		graphFactory.set(graph);
		graphFactory.set(dimensionsOfToroid);	
		graphFactory.makeVerticesForGraph();// testing this method	
		String expectedValue = "0x0"; 
		String actualValue =  graph.getVertices().get(0).getId();
	
		System.out.println("LINE 72 GFTEst nowType?"+graph.getVertices().get(0).getNowType());
		assertEquals(expectedValue, actualValue);
	
		
		
	}

	/**
	 * This test would effectively need to demonstrate that every single
	 * vertex in the graph has had its neighbourhood attached.
	 */
	@Test
	public void testAttachAllNeighbourhoods() {
		
		int dimensionsOfToroid = 5;
		graph = new Graph();
		graph.set(dimensionsOfToroid);
		graphFactory = new GraphFactory();
		graphFactory.set(graph);
		graphFactory.set(dimensionsOfToroid);
		graphFactory.initDirection8Pairs();
		graphFactory.makeVerticesForGraph();	
		graphFactory.attachAllNeighbourhoods();//testing this method
		for (Vertex vertex : graph.getVertices()) {

			Map<String,Vertex> actualMap = vertex.getNeighbourhood();
			Set<String> actualKeySet = actualMap.keySet();
			Set<String> expectedKeySet = new HashSet<>();
			expectedKeySet.add("N");
			expectedKeySet.add("NE");
			expectedKeySet.add("E");
			expectedKeySet.add("SE");
			expectedKeySet.add("S");
			expectedKeySet.add("SW");
			expectedKeySet.add("W");
			expectedKeySet.add("NW");
		
			assertTrue(actualKeySet.containsAll(expectedKeySet));
		
		}
		
	}

	/**
	 * Takes a vertex and sets its nextVertexInScanChain field
	 * to the vertex that is attached at its east adjacent position.
	 * 
	 * This relies on two things created first (a) the vertices and
	 * (b) the adjacent vertex at position "E" should already be 
	 * attached.
	 * 
	 */
	@Test
	public void testAttachScanChainOf_Vertex() {
		
		int dimensionsOfToroid = 5;	
		graphFactory = new GraphFactory();
		graph = new Graph();
		graphFactory.set(graph);
		graphFactory.set(dimensionsOfToroid);
		graphFactory.initDirection8Pairs();
		graphFactory.makeVerticesForGraph();
		graphFactory.attachAllNeighbourhoods();
		vertex = graph.getVertexWithId("1x1");
		graphFactory.attachScanChainOf(vertex);//testing this method
		System.out.println(vertex.getNeighbourhood());
		Vertex actualVertex = vertex.getNextVertexInScanChain();		
		String actualValue = actualVertex.getId();
		String expectedValue = "2x1";

		assertEquals(expectedValue, actualValue);

		vertex2 = graph.getVertexWithId("4x1");
		graphFactory.attachScanChainOf(vertex2);//testing this method
		System.out.println(vertex2.getNeighbourhood());
		Vertex actualVertex2 = vertex2.getNextVertexInScanChain();		
		String actualValue2 = actualVertex2.getId();
		String expectedValue2 = "0x2";
	
		assertEquals(expectedValue2, actualValue2);

	}

}