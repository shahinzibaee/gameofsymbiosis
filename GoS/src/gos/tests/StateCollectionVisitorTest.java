package gos.tests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import gos.factories.GraphFactory;
import gos.graph.Graph;
import gos.graph.Vertex;
import gos.visitors.StateCollectionVisitor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StateCollectionVisitorTest {

	private Graph graph;
	private GraphFactory graphFactory;
	private StateCollectionVisitor stateCollectionVisitor;
	private final String idOfStartVertex = "0x0";
	private Vertex currentVertex;
	private int dimensionsOfToroid = 2;

	@Before
	public void setUp() throws Exception {
	
		graph = new Graph();
		graphFactory = new GraphFactory(graph);
		graphFactory.set(dimensionsOfToroid);  
		graphFactory.buildGraph();
		currentVertex = graph.getVertexWithId(idOfStartVertex);
		stateCollectionVisitor = new StateCollectionVisitor(graph);
				
	}

	@After
	public void tearDown() throws Exception {
	
		graph = null;
		graphFactory = null;
		currentVertex = null;
		stateCollectionVisitor = null;
		
	}

	@Test
	public void testStateCollectionVisitor() {
		
		String actualVertex = stateCollectionVisitor.getGraph().getVertexWithId("0x0").toString();
		String expectedVertex = "[0x0]";
		assertEquals(actualVertex, expectedVertex);
		
	}
	
	/**
	 * is the last method in the call-chain (called by 
	 * buildSumsOfNeighbourhoods), therefore is the first test
	 * that should pass for this class.
	 */
	@Test
	public void testCalculateSumOfOnNeighboursForCurrentVertex() {

		graph.getVertexWithId("0x0").setNowState("off");
		graph.getVertexWithId("0x1").setNowState("on");
		graph.getVertexWithId("1x1").setNowState("on");
		graph.getVertexWithId("1x0").setNowState("on");
		currentVertex = graph.getVertexWithId("0x0");
		int actualValue = stateCollectionVisitor.calculateSumOfOnNeighboursFor(currentVertex);
		int expectedValue = 8;
		assertEquals(expectedValue, actualValue);
/*		
		graph.getVertexWithId("0x0").setNowState("off");
		graph.getVertexWithId("0x1").setNowState("off");
		graph.getVertexWithId("1x1").setNowState("on");
		graph.getVertexWithId("1x0").setNowState("on");
		currentVertex = graph.getVertexWithId("0x0");
		stateCollectionVisitor.set(currentVertex);
		int actualValue2 = stateCollectionVisitor.calculateSumOfOnNeighboursForCurrentVertex();
		int expectedValue2 = 6;
		assertEquals(expectedValue2, actualValue2);

		graph.getVertexWithId("0x0").setNowState("on");
		graph.getVertexWithId("0x1").setNowState("off");
		graph.getVertexWithId("1x1").setNowState("on");
		graph.getVertexWithId("1x0").setNowState("on");
		int actualValue3 = stateCollectionVisitor.calculateSumOfOnNeighboursForCurrentVertex();
		int expectedValue3 = 6;
		assertEquals(expectedValue3, actualValue3);
*/
	}
	
	/**
	 * Called by getSumsOfNeighbourhoods. 
	 * Calls calculateSumsOfNeighbourhoodsForCurrentVertex().
	 */
	@Test
	public void testMakeSumsOfNeighbourhoods() {
		
		Map<String,Vertex> neighbourhood = new HashMap<>();
		neighbourhood.put("N",graph.getVertexWithId("0x1"));
		neighbourhood.put("NE",graph.getVertexWithId("1x1"));
		neighbourhood.put("E",graph.getVertexWithId("1x0"));
		neighbourhood.put("SE",graph.getVertexWithId("1x1"));
		neighbourhood.put("S",graph.getVertexWithId("0x1"));
		neighbourhood.put("SW",graph.getVertexWithId("1x1"));
		neighbourhood.put("W",graph.getVertexWithId("1x0"));
		neighbourhood.put("NW",graph.getVertexWithId("1x1"));
		currentVertex.setNeighbourhood(neighbourhood);		
		
		graph.getVertexWithId("0x0").setNowState("off");
		graph.getVertexWithId("0x1").setNowState("on");
		graph.getVertexWithId("1x1").setNowState("on");
		graph.getVertexWithId("1x0").setNowState("on");
		
		Map<Vertex, Integer> expectedSumsOfNeighbourhoods = new HashMap<>(); 
		expectedSumsOfNeighbourhoods.put(graph.getVertexWithId("0x0"), 8);
		expectedSumsOfNeighbourhoods.put(graph.getVertexWithId("1x0"), 6);
		expectedSumsOfNeighbourhoods.put(graph.getVertexWithId("0x1"), 6);
		expectedSumsOfNeighbourhoods.put(graph.getVertexWithId("1x1"), 4);

		assertEquals(expectedSumsOfNeighbourhoods, stateCollectionVisitor.makeSumsOfNeighbourhoods());
				
	}

}
