package gos.tests;

import static org.junit.Assert.*;

import gos.GUI.GUI;
import gos.factories.GraphFactory;
import gos.graph.Graph;
import gos.main.InputOutput;
import gos.main.Simulation;
import gos.main.UserInput;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/*
 * Gui window a fraction of a second after it is opened from JUnit. 
 * Not sure why or how to prevent it. It makes no difference if I
 * comment out 'setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);' which 
 * is in Gui's constructor. It makes no difference if I make launchGui() 
 * a static method and run it from @BeforeClass setUpClass().
 * The only time the window remains, is when launchGui() is replaced with
 * psvm(String[] args).
 */

public class GuiTest {

	private Graph graph;
	private GraphFactory graphFactory;
	private UserInput userInput;
	private Simulation simulation;
	private GUI gui;
	private InputOutput io;
	
	@Before
	public void setUp() throws Exception {
	
		graph = new Graph();
		graphFactory = new GraphFactory(graph);
		graphFactory.setDefaultDimensionsOfToroid();
		graphFactory.buildGraph();
		simulation = new Simulation(graph);
		io = new InputOutput();
		userInput = new UserInput(graph,io, simulation);
		gui = new GUI();
		gui.set(graph, userInput); 
		
	}

	@After
	public void tearDown() throws Exception {
	
		graph = null;
		graphFactory = null;
		simulation = null;
		userInput = null;
		gui = null;
	
	}

	@Test
	public void testGui() {
		fail("Not yet implemented");
	}


}
