package gos.tests;

import static org.junit.Assert.*;
import gos.factories.GraphFactory;
import gos.graph.Graph;
import gos.graph.Vertex;
import gos.visitors.UpdateStateVisitor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UpdateStateVisitorTest {

	Graph graph;
	GraphFactory graphFactory;
	UpdateStateVisitor updateStateVisitor;
	Vertex vertex;
	
	@Before
	public void setUp() throws Exception {
	
		graph = new Graph();	
		graphFactory = new GraphFactory(graph);
		graphFactory.setDefaultDimensionsOfToroid();//sets value in both Graph and GraphFactory to defaults
		graphFactory.buildGraph();
		updateStateVisitor = new UpdateStateVisitor(graph);
		
	}

	@After
	public void tearDown() throws Exception {
	
		graph = null;
		graphFactory = null;
		updateStateVisitor = null;
	
	}

	@Test
	public void testUpdateStateVisitor() {

		assertNotNull(updateStateVisitor.getStateCollectionVisitor());
		assertNotNull(updateStateVisitor.getStateChangeVisitor());
		assertNotNull(updateStateVisitor.getStateCollectionVisitor().makeSumsOfNeighbourhoods());
		String expectedValue = "[0x0]";
		String actualValue = updateStateVisitor.getGraph().getVertexWithId("0x0").toString();
		assertEquals(expectedValue, actualValue);
		
	}

	@Test
	public void testUpdateNextStates() {

		updateStateVisitor = new UpdateStateVisitor(graph);
		updateStateVisitor.updateNextStates();
		assertNotNull(graph.getVertexWithId("0x0").getNextState());

	}

	@Test
	public void testUpdateNowStates() {
		
		vertex = graph.getVertexWithId("0x0");
		vertex.setNextState("off");
		updateStateVisitor.updateNowStates();
		String actualValue = vertex.getNowState();
		String expectedValue = "off";
		assertEquals(expectedValue, actualValue);
		
		vertex = graph.getVertexWithId("0x0");
		vertex.setNextState("off");
		updateStateVisitor.updateNowStates();
		String actualValue2 = vertex.getNowState();
		String expectedValue2 = "off";
		assertEquals(expectedValue2, actualValue2);
		
	}

}